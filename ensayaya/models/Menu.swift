import Foundation

struct Menu:Decodable{
    let idmenu:String
    let view:String
    let label:String
    let systemimage:String
}
