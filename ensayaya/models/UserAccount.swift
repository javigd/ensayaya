import Foundation
import SwiftUI

struct UserAccount:Decodable{
    
    let iduseraccount:Int
    let username:String
    let password:String
    let email:String
    let active:Bool	
    
}
