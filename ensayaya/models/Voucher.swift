import Foundation


struct Voucher:Decodable, Hashable{
    
    let idvoucher:String
    let name:String
    let usages:String
    let price:String
}
