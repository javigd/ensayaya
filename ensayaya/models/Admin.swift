import Foundation


struct Admin:Decodable{
    
    let idadmin:Int
    let name:String
    let surname:String
    let phone:String
    let dni:String
    let address:String
    let useraccount_id:Int
}
