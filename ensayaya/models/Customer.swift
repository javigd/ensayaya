
import Foundation

struct Customer:Decodable{
    
    let idcustomer:Int
    let name:String
    let surname:String
    let phone:String
    let birthday:Date
    let useraccount_id:Int
    
    
}
