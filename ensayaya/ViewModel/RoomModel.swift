import Foundation

class RoomModel:ObservableObject{
    @Published var rooms = [Room]() 
    
    //En este init, podrían ir todos los métodos de este ViewModel, pero si queremos llamarlos de una manera más personalizada en las vistas, usamos el onAppear (también explicado en ListRoomView)
    /*init(){
        self.allRooms()
    }*/
    
    func allRooms() { 
        RestService<[Room]>().get(endpoint: .room) { result in
            switch result {
            case .success(let rooms):
                self.rooms = rooms
            case .failure(let error):
                let a = 1
            }
        }
    }
}
