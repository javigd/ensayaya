//
//  UpdateUserAccountView.swift
//  ensayaya
//
//  Created by LDTS Educacion on 24/3/22.
//

import SwiftUI

struct UpdateUserAccountView: View {
    
    @State private var birthday = Date()
    
    var body: some View {
        VStack(spacing:32){
            VStack(spacing:16){
                InputTextFieldView(text: .constant(""), placeholder: "Nombre", keyboardType: .namePhonePad, sfSymbol: nil)
                InputTextFieldView(text: .constant(""), placeholder: "Apellidos", keyboardType: .namePhonePad, sfSymbol: nil)
                DatePicker("FechaNacimiento", selection:$birthday, displayedComponents: [.date]).datePickerField()
                Divider()
                InputTextFieldView(text: .constant(""), placeholder: "Nombre de Usuario", keyboardType: .namePhonePad, sfSymbol: "person.fill")
                InputPasswordView(password: .constant(""), placeholder: "Contraseña", sfSymbol: "lock")
                InputPasswordView(password: .constant(""), placeholder: "Confirmar Contraseña", sfSymbol: "lock")
                InputTextFieldView(text: .constant(""), placeholder: "Email", keyboardType: .emailAddress, sfSymbol: "envelope")
            }
            
            ButtonView(title:"Editar Cuenta"){
                
            }
            
        }.padding(.horizontal, 15)
    }
}

struct UpdateUserAccountView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateUserAccountView()
    }
}
