import SwiftUI

struct MyAccountView: View {
    
    let name:String = "Javi"
    let date:Date = Date()
    var body: some View {
        
        NavigationView{
            VStack(alignment: .leading, spacing: 32){
                
                Text("Nombre y apellidos: \(name) Giráldez")
                Text("Teléfono de contacto: 612123123")
                Text("Email de contacto: javier@gmail.com")
                HStack{
                    Text("Fecha de nacimiento: ")
                    Text(date, style:.date) //Podemos usar más estilos como .time, .relative....
                }
                Text("Nombre de Usuario: javi")
                NavigationLink(destination: UpdateUserAccountView(), label:{
                    ImpostorButton(title:"Editar Cuenta")
                })
                Spacer()
            }
        }
        .padding()
        .navigationTitle("Mi Cuenta")
    }
}

struct MyAccountView_Previews: PreviewProvider {
    static var previews: some View {
        MyAccountView()
    }
}
