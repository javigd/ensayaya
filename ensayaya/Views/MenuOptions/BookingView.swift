import SwiftUI

struct BookingView: View {
    
    @StateObject var roomModel = RoomViewModel()
    @StateObject var materialModel = MaterialViewModel()
    @StateObject var voucherModel = VoucherViewModel()
    @State var roomSelected:Room
    @State var materialSelected:Material
    @State var voucherSelected:Voucher
    @State private var materialOn = false
    @State private var voucherOn = false
    
    var body: some View {
        VStack(alignment: .leading, spacing:16){
            Text("Reserva de sala")
                .font(.title)
                .multilineTextAlignment(.center)
                .padding()
            HStack{
                Text("Selecciona una sala: ")
                Picker(selection:$roomSelected, label:Text("Selecciona una sala")){
                        ForEach(roomModel.rooms, id:\.self){ room in
                            Text(room.name)
                    }
                }.pickerStyle(MenuPickerStyle())
            }
            //Aquí irían situadas las diferentes horas de empezar a ensayar. Creo que lo más fácil es elegir la hora de empezar y  que puedan elegir cuántas horas van a ensayar (1,2,3,4...) Manejo de horas algo complicado.
            Toggle("¿Desea alquilar algún instrumento?", isOn: $materialOn)
            if materialOn == true{
                Text("Selecciona los instrumentos a alquilar: ")
                Picker(selection:$materialSelected, label:Text("Selecciona los instrumentos a alquilar")){
                        ForEach(materialModel.materials, id:\.self){ material in
                            Text(material.name)
                    }
                }.pickerStyle(MenuPickerStyle())
            }
            Toggle("¿Quiere usar alguno de sus bonos?", isOn:$voucherOn)
            if voucherOn == true{
                Text("Selecciona uno de tus bonos disponibles: ")
                Picker(selection:$voucherSelected, label:Text("Selecciona uno de tus bonos disponibles")){
                        ForEach(voucherModel.vouchers, id:\.self){ voucher in
                            Text(voucher.name)
                    }
                }.pickerStyle(MenuPickerStyle())
            }
            ButtonView(title: "Reservar"){
                //Aquí va un método del modal
                //Aquí tengo una duda, en caso de que no seleccionemos ningún bono ni ningún instrumento, como en el picker está seleccionado uno de por sí, cómo hacemos para no mandarlo? Uso los Bool esos con un if y llama a diferentes métodos?
            }
        }
        .padding()
        .onAppear{
            roomModel.allRooms()
            materialModel.allMaterials()
            voucherModel.allVouchers()
        }
        Spacer()
    }
    
}

struct BookingView_Previews: PreviewProvider {
    static var previews: some View {
        BookingView(roomSelected:Room(idroom: "1", name: "Sala 1", description: "Sala de 8x4 totalmente equipada"), materialSelected: Material(idmaterial: "1", name: "Yamaha"), voucherSelected: Voucher(idvoucher: "1", name: "Bono 10", usages: "10", price: "60€"))
    }
}
