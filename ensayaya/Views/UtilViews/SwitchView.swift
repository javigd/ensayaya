import SwiftUI

struct SwitchView: View {
    
    let menu:String
    
    var body: some View {
        switch menu {
        case "MyAccountView()":
            MyAccountView()
        case "MyBookingsView()":
            MyBookingsView()
        case "BookingView()":
            BookingView(roomSelected:Room(idroom: "1", name: "Sala 1", description: "Sala de 8x4 totalmente equipada"), materialSelected: Material(idmaterial: "1", name: "Yamaha"), voucherSelected: Voucher(idvoucher: "1", name: "Bono 10", usages: "10", price: "60€"))
        default:
            SignInView()
        }
    }
}

struct SwitchView_Previews: PreviewProvider {
    static var previews: some View {
        SwitchView(menu: "MyAccountView()")
    }
}
