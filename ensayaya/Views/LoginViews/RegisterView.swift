//
//  RegisterView.swift
//  ensayaya
//
//  Created by LDTS Educacion on 14/3/22.
//

import SwiftUI

struct RegisterView: View {
    
    @State private var birthday = Date()
    
    var body: some View {
        NavigationView{
            
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: .constant(""), placeholder: "Nombre", keyboardType: .namePhonePad, sfSymbol: nil)
                    InputTextFieldView(text: .constant(""), placeholder: "Apellidos", keyboardType: .namePhonePad, sfSymbol: nil)
                    DatePicker("FechaNacimiento", selection:$birthday, displayedComponents: [.date]).datePickerField()
                    Divider()
                    InputTextFieldView(text: .constant(""), placeholder: "Nombre de Usuario", keyboardType: .namePhonePad, sfSymbol: "person.fill")
                    InputPasswordView(password: .constant(""), placeholder: "Contraseña", sfSymbol: "lock")
                    InputPasswordView(password: .constant(""), placeholder: "Confirmar Contraseña", sfSymbol: "lock")
                    InputTextFieldView(text: .constant(""), placeholder: "Email", keyboardType: .emailAddress, sfSymbol: "envelope")
                }
                
                ButtonView(title:"Registrarse"){
                    
                }
                
            }.padding(.horizontal, 15)
            .navigationTitle("Nueva Cuenta")
            .applyClose()
        }
    }
}

extension View { //Extensión de la vista para darle algo de formato a campos que no utilizan ninguna de las UtilViews. Es como un método.
    public func datePickerField() -> some View {
        return self
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .background(Color.white)
            .frame(height: 44)
            .cornerRadius(10)
    }
}


struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
