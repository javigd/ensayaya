//
//  RememberPasswordView.swift
//  ensayaya
//
//  Created by LDTS Educacion on 14/3/22.
//

import SwiftUI

struct RememberPasswordView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        
        NavigationView{
            
            VStack(spacing:16){
                
                InputTextFieldView(text: .constant(""), placeholder: "Email", keyboardType: .emailAddress, sfSymbol: "envelope")
                
                ButtonView(title:"Enviar email"){
                    presentationMode.wrappedValue.dismiss()
                }
                
            }.padding(.horizontal, 15)
                .navigationTitle("Recordar Contraseña")
                .applyClose()
            
        }
    }
}

struct RememberPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        RememberPasswordView()
    }
}
