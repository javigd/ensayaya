import SwiftUI

struct ListRoomView: View {
    
    @StateObject var roomModel = RoomViewModel() //Usamos StateObject para representar los datos que nos llegan a nuestro ViewModel en esta vista. EnvironmentObject también nos serviría, pero en este caso, no vamos a propagar estos cambios por muchos sitios de nuestra App, así que ahorramos recursos.
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                   ForEach(roomModel.rooms, id:\.idroom){ room in
                       Text(room.name)
                           .font(.title)
                           .fontWeight(.semibold)
                           .multilineTextAlignment(.center)
                       Image("default_rehearsal_room").resizable().scaledToFit().padding()
                       Text(room.description)
                           .padding([.leading, .bottom, .trailing])
                   }
                }
                .padding(.top,50)
                .onAppear{
                     roomModel.allRooms() //Usamos onAppear, para que al aparecer esta vista, nos cargue el método en concreto que queramos. De esta manera, evitamos hacer un init en los diferentes ViewModels que contenga todos los métodos de esa clase (explicado también en RoomModel)
                }
                .navigationTitle("Lista de Salas")
            }.applyClose()
        }
    }
}

struct ListRoomView_Previews: PreviewProvider {
    static var previews: some View {
        ListRoomView()
    }
}
