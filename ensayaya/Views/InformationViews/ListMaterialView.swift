import SwiftUI

struct ListMaterialView: View {
    
    @StateObject var materialViewModel = MaterialViewModel()
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment:.leading, spacing:16){
                   ForEach(materialViewModel.materials, id:\.idmaterial){ material in
                       Text(material.name)
                           .font(.title)
                           .fontWeight(.semibold)
                   }
                }
                
                .padding(.top,50)
                .onAppear{
                     materialViewModel.allMaterials()
                }
                .navigationTitle("Lista de Instrumentos")
            }.applyClose()
        }
    }
}

struct ListMaterialView_Previews: PreviewProvider {
    static var previews: some View {
        ListMaterialView()
    }
}
