import SwiftUI

struct SideMenuOptionView: View {
    
    let sideMenuViewModel:SideMenuViewModel
    
    var body: some View {
        VStack{
            HStack(spacing:16){
                Image(systemName:sideMenuViewModel.imageName).frame(width:24, height:24)
                Text(sideMenuViewModel.title).font(.system(size: 18, weight: .semibold))
                Spacer()
            }.padding()
        }
    }
}

struct SideMenuOptionView_Previews: PreviewProvider {
    static var previews: some View {
        SideMenuOptionView(sideMenuViewModel: .booking)
    }
}
