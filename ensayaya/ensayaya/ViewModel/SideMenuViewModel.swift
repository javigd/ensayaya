import Foundation

class SideMenuViewModel:ObservableObject{
    @Published var menuOptions = [Menu]()
    
    func menuCustomerOptions() {
        RestService<[Menu]>().get(endpoint: .menuCustomer) { result in
            switch result {
            case .success(let menu):
                self.menuOptions = menu
            case .failure(let error):
                let a = 1
            }
        }
    }
}


//Simulación
/*
enum SideMenuViewModel:Int, CaseIterable{
    
    case myAccount
    case myBookings
    case booking
    case logout
    
    
    var title:String{
        switch self{
        case .myAccount: return "Mi Cuenta"
        case .myBookings: return "Mis Reservas"
        case .booking: return "Reservar Sala"
        case .logout: return "Cerrar Sesión"
        }
    }
    var imageName:String{
        switch self{
        case .myAccount: return "person.fill"
        case .myBookings: return "calendar"
        case .booking: return "key.fill"
        case .logout: return "arrow.backward.circle.fill"
        }
    }
 */
//}

