//
//  RestService.swift
//  ensayaya
//
//  Created by LDTS Educacion on 12/3/22.
//

import Foundation
import SwiftUI

enum RestMethod: String {
    case get = "GET", post = "POST", delete = "DELETE", put = "PUT"  //Read, Create, delete, update
}

enum Endpoints: String {  //Para acceder a los servicios correspondientes, esto es elegido en el ViewModel correspondiente
    case room = "/salas.php"
    case login = "/login.php"
    case voucher = "/vouchers.php"
    case material = "/materials.php"
    case menuCustomer = "/menu.php"
}

class RestService<T: Decodable> {
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder() //Variable para decodificar el JSON
        let formatter = DateFormatter()
        formatter.dateFormat = "dd//mm/yyyy'T'HH:MM:ss" //Aquí lo que hago es decodificar correctamente las fechas y el snakeCase --> room_id por ejemplo. No uso CamelCase
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(formatter)
        return decoder
    }()
    
    func get(endpoint: Endpoints, _ completion: ((_ result: Result<T, ApiError>) -> Void)? = nil) {
        request(url: endpoint.rawValue, method: .get, body: nil, completion)
    }
    
    func post(endpoint: Endpoints, params:[String:Any],_ completion: ((_ result: Result<T, ApiError>) -> Void)? = nil){
        request(url: endpoint.rawValue, method: .post, body: params, completion)
    }
    
   //Para el delete envío directamente la url por request: Ejemplo
    /*
     Endpoint.reservasDelete.rawValue = /reservas?id=%@
     request(url: String(format: Endpoint.reservasDelete.rawValue, id), method: .delete,  body: nil, completion)
     */
    
    func request(url: String, method: RestMethod, body: [String: Any]? = nil, _ completion: ((_ result: Result<T, ApiError>) -> Void)? = nil) {
        let prefixUrl = "https://ensayaya.javiergddw.com"
        guard let urlFinal = URL(string: String(format: "%@%@", prefixUrl, url)) else {
            self.callCompletion(completion: completion, result: .failure(ApiError(message:"")))
            return
        }
        //El request aquí sale vacío
        var request = URLRequest(url: urlFinal)
        request.httpMethod = method.rawValue
        /*if method == .post, let b = body, let json: Data = b.toJson() {
            // necesitas una biblioteca q convierta un Dictionary en json
            request.httpBody = json
        }*/
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                self.callCompletion(completion: completion, result: .failure(ApiError(message:"")))
                return
            }
            
            guard let res = response as? HTTPURLResponse else {
                self.callCompletion(completion: completion, result: .failure(ApiError(message:"")))
                return
            }
            
            let status = res.statusCode
            if status >= 200 && status < 300 {
                do {
                    let model = try self.decoder.decode(T.self, from: data)
                    self.callCompletion(completion: completion, result: .success(model))
                } catch {
                    self.callCompletion(completion: completion, result: .failure(ApiError(message:"")))
                }
            } else {
                self.callCompletion(completion: completion, result: .failure(ApiError(message:"")))
            }
        }.resume()
    }
    
    private func callCompletion(completion: ((Result<T, ApiError>) -> Void)?, result: Result<T, ApiError>) {
        DispatchQueue.main.async {
            completion?(result)
        }
    }
}
struct ApiError: Error {
    let message: String
}


