import SwiftUI

struct SideMenuView: View {
    
    @Binding var isShowingMenu:Bool
    @StateObject var sideMenuViewModel = SideMenuViewModel()
    
    var body: some View {
        ZStack{
            LinearGradient(gradient: Gradient(colors: [Color(red: 0 / 255, green: 0 / 255, blue: 0 / 255), Color(red: 43 / 255, green: 43 / 255, blue: 43 / 255)]), startPoint: .top, endPoint: .bottom).ignoresSafeArea()
            //Opcional lo de arriba
            VStack{
                SideMenuHeaderView(isShowingMenu: $isShowingMenu).foregroundColor(Color.white)
                
                ForEach(sideMenuViewModel.menuOptions, id: \.idmenu) { menu in
                    NavigationLink(destination: SwitchView(menu: menu.view), label: {
                        HStack(spacing:16){
                            Image(systemName:menu.systemimage).frame(width:24, height:24)
                            Text(menu.label).font(.system(size: 18, weight: .semibold))
                            Spacer()
                        }
                        .padding()
                        .foregroundColor(Color.white)
                    }
                )}
                Spacer()
            }.onAppear{
                sideMenuViewModel.menuCustomerOptions()}
        }.navigationBarHidden(true)
    }
}

struct SideMenuView_Previews: PreviewProvider {
    static var previews: some View {
        SideMenuView(isShowingMenu: .constant(true))
    }
}
