import SwiftUI

struct ListVoucherView: View {
    
    @StateObject var voucherModel = VoucherViewModel()
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment:.leading, spacing:16){
                   ForEach(voucherModel.vouchers, id:\.idvoucher){ voucher in
                       Text(voucher.name)
                           .font(.title)
                           .fontWeight(.semibold)
                       Text("Usos:  \(voucher.usages)")
                       Text("Precio:  \(voucher.price)€")
                       ButtonView(title: "Comprar"){
                           //Método para comprar el bono, sería un INSERT en customer_has_bonos
                       }
                   }
                }
                .padding()
                .padding(.top,50)
                .onAppear{
                     voucherModel.allVouchers() 
                }
                .navigationTitle("Lista de Bonos")
            }.applyClose()
        }
    }
}

struct ListVoucherView_Previews: PreviewProvider {
    static var previews: some View {
        ListVoucherView()
    }
}
