import SwiftUI

struct SignInView: View {
    @State var username = ""
    @State var password = ""
    @State private var showRegistration = false //Variables que son modificadas dinámicamente en la vista --> Propiedad @State (su cambio no es propagado a vistas diferentes)
    @State private var showRememberPassword = false
    @EnvironmentObject var loginViewModel:LoginViewModel
    
    var body: some View {
        
        VStack(spacing:16){
            
            Image("logo").resizable().scaledToFit()
            
            VStack(spacing:16){
                InputTextFieldView(text: $username, placeholder: "Nombre de Usuario", keyboardType: .namePhonePad, sfSymbol: "person.fill")
                InputPasswordView(password: $password, placeholder: "Contraseña", sfSymbol: "lock")
            }
            HStack{
                Spacer()
                Button(action:{
                    showRememberPassword.toggle() //Un toggle nos permite cambiar el valor de un booleano automáticamente
                }, label:{
                    Text("¿Olvidaste tu contraseña?")
                })
                    .font(.system(size:16, weight:.bold))
                    .sheet(isPresented:$showRememberPassword, content:{
                        RememberPasswordView()//Al cambiar la variable a true, nos muestra el modal o sheet.
                    })
            }
            ButtonView(title: "Iniciar Sesión"){
                guard !username.isEmpty, password.isEmpty else{
                    return
                }
                loginViewModel.signIn(username: username, password: password)
            }
            ButtonView(title: "Registrarse", background: .white, foreground: .black, border: .black){
                showRegistration.toggle() //Al cambiar la variable a true, nos muestra el modal o sheet.
            }.sheet(isPresented:$showRegistration, content:{
                RegisterView()
            })
        }
        .padding(.horizontal, 15)
        .navigationTitle("Iniciar Sesión")
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
