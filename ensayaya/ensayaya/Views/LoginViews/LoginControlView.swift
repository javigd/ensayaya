import SwiftUI

struct LoginControlView: View {
    
    @EnvironmentObject var loginViewModel:LoginViewModel
    
    var body: some View {
        ZStack{
            if loginViewModel.signedIn {
                HomeView()
            }
            else{
                SignInView()
            }
        }
        .onAppear {
            loginViewModel.signedIn = loginViewModel.isSignedIn
        }
    }
}

struct LoginControlView_Previews: PreviewProvider {
    static var previews: some View {
        LoginControlView()
    }
}
