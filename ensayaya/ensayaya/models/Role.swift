import Foundation

struct Role:Decodable{
    let idrole:Int
    let rolename:String
    let roledescription:String
}
