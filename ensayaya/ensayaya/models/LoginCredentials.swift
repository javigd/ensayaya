import Foundation
import SwiftUI

struct LoginCredentials:Decodable{
    
    let username:String
    let password:String
    
}
