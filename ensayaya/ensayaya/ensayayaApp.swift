import SwiftUI

@main
struct ensayayaApp: App {
    var body: some Scene {
        WindowGroup {
            let loginViewModel = LoginViewModel()
            LoginControlView().environmentObject(loginViewModel)
            
        }
    }
}
