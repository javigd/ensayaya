import SwiftUI

//UNIFICAR LAS TABLAS DE USUARIOS, VER SI SE PUEDE HACER COMO ESTÁ LA BASE DE DATOS AHORA MISMO

struct AdminClientView: View {
    
    @StateObject var userDataViewModel = UserDataViewModel()
    @StateObject var voucherViewModel = VoucherViewModel()
    @State var idUser:String
    @State var name:String
    @State var surname:String
    @State var phone:String
    @State var email:String
    @State var activo:Bool
    @State var role:String
    @State var showBookings:Bool = false;
    @State var showBookingMaterials:Bool = false;
    
    var body: some View {
        VStack(spacing:16){
            Text("Nombre y apellidos: \(name) \(surname)")
            Text("Teléfono de contacto:  \(phone)")
            Text("Email: \(email)")
            if (activo == true){
                ButtonView(title:"Desactivar cuenta"){
                    userDataViewModel.disableAccount(id: idUser)
                }
            }else{
                ButtonView(title:"Activar cuenta"){
                    userDataViewModel.enableAccount(id: idUser)
                }
            }
            if (userDataViewModel.messageError != ""){
                Text(userDataViewModel.messageError).padding()
            }
            if(role == "cliente"){
                ButtonView(title:"Ver reservas"){
                    showBookings.toggle()
                }.sheet(isPresented:$showBookings, content:{
                    AdminBookingsView(idUser:idUser, name:name, surname:surname)
                })
                ButtonView(title:"Añadir materiales"){
                    showBookingMaterials.toggle()
                }.sheet(isPresented:$showBookingMaterials, content:{
                    BookingMaterialsView(idUser: idUser)
                })
                Text("Bonos activos").font(.title3)
                ForEach(voucherViewModel.uVoucher, id:\.id){ voucher in
                    Text("Tipo de bono: \(voucher.name)")
                    Text("Número de usos restantes: \(voucher.numUsages)")
                    Text("Usos totales del bono: \(voucher.usages)")
                }
            }
        }.padding().onAppear{
            voucherViewModel.voucherPerId(idUser: idUser)
        }
    }
}

struct AdminClientView_Previews: PreviewProvider {
    static var previews: some View {
        AdminClientView(idUser: "1", name: "Javi", surname: "Giráldez", phone: "600000000", email: "correo@gmail.com", activo: true, role:"cliente")
    }
}
