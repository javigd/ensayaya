import SwiftUI

struct NewAdminView: View {
    
    private var requirements: Array<Bool> {
        [
            password.count >= 7,             // Longitud mínima
            password &= "[0-9]",             // Al menos un número
            password &= ".*[^A-Za-z0-9].*"   // Al menos un caracter especial
        ]
    }
    
    private var mobReque:Array<Bool>{
        [
            phone.count == 9,
            String(phone.prefix(1)) &= "[6-7]"
        ]
    }
    
    @StateObject var authenticationViewModel = AuthenticationViewModel()
    @State var name:String = ""
    @State var surname:String = ""
    @State var username:String = ""
    @State var phone:String = ""
    @State var email:String = ""
    @State var password:String = ""
    @State var confirmPassword:String = ""
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre", keyboardType: .emailAddress, sfSymbol: "person")
                    InputTextFieldView(text: $surname, placeholder: "Apellidos", keyboardType: .emailAddress, sfSymbol: "person")
                    InputTextFieldView(text: $phone, placeholder: "Número de teléfono", keyboardType: .phonePad, sfSymbol: "phone")
                    InputTextFieldView(text: $email, placeholder: "Email", keyboardType: .namePhonePad, sfSymbol: "envelope")
                    InputTextFieldView(text: $username, placeholder: "Nombre de usuario", keyboardType: .namePhonePad, sfSymbol: "envelope")
                    InputPasswordView(password: $password, placeholder: "Contraseña", sfSymbol: "lock")
                    InputPasswordView(password: $confirmPassword, placeholder: "Confirmar Contraseña", sfSymbol: "lock")
                }
                
                ButtonView(title:"Registrarse"){
                    authenticationViewModel.createNewAdmin(name:name, surname:surname, phone:phone, email: email, password: password, username:username)
                }.disabled(password != confirmPassword || name.isEmpty || surname.isEmpty || phone.isEmpty || email.isEmpty || username.isEmpty || self.requirements[0] == false || self.requirements[1] == false || self.requirements[2] == false || self.mobReque[0] == false || self.mobReque[1] == false)
                if let messageError = authenticationViewModel.messageError {
                    Text(messageError).bold().font(.body).foregroundColor(.red).padding(.top, 20)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Nueva Cuenta")
            .applyClose()
        }
    }
}

struct NewAdminView_Previews: PreviewProvider {
    static var previews: some View {
        NewAdminView()
    }
}
