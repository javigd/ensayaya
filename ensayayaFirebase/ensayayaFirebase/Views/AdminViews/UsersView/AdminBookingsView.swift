import SwiftUI

struct AdminBookingsView: View {
    
    @StateObject var bookingViewModel = BookingViewModel()
    @State var idUser:String
    @State var name:String
    @State var surname:String
    
    var body: some View {
        
        NavigationView{
            ScrollView{
                VStack(alignment:.leading, spacing:16){
                        ForEach(bookingViewModel.bookings, id:\.id){ booking in
                            Text("\(booking.idRoom) - Fecha: \(booking.FechaReserva, style:.date) \(booking.horaInicio):00 - \(booking.horaFin):00").padding()
                            ForEach(booking.materials ?? [""], id:\.self){ material in
                                Text(material).padding()
                            }
                        }
                    }
                .padding(.top,50)
                .onAppear{
                    bookingViewModel.bookingsByUser(id: idUser)
                }
                .navigationTitle("Reservas del usuario \(name) \(surname)")
            }.applyClose()
        }
    }
}

struct AdminBookingsView_Previews: PreviewProvider {
    static var previews: some View {
        AdminBookingsView(idUser: "1", name: "Javi", surname: "Giráldez")
    }
}
