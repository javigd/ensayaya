import SwiftUI

struct BookingMaterialsView: View {
    
    @StateObject var materialViewModel = MaterialViewModel()
    @StateObject var bookingViewModel = BookingViewModel()
    @State var idUser:String
    @State var selectKeeper = Set<String>()
    
    var body: some View {
        NavigationView{
        VStack(spacing: 15) {
            List(selection: $selectKeeper) {
                ForEach(materialViewModel.materialsName, id:\.self) { material in
                    Text(material)
                }
            }
            if(bookingViewModel.messageError != ""){
                Text(bookingViewModel.messageError)
            }
            ButtonView(title:"Reservas materiales"){
                bookingViewModel.bookingMaterial(idUser:idUser, materials:selectKeeper)
            }.padding()
        .navigationBarItems(trailing: EditButton())
        .onAppear{materialViewModel.getAllActiveMaterials(string:true)}
            }
        }
    }
}

struct BookingMaterialsView_Previews: PreviewProvider {
    static var previews: some View {
        BookingMaterialsView(idUser:"1")
    }
}

