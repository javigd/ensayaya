import SwiftUI

struct AdminsView: View {
    
    @StateObject var userDataViewModel = UserDataViewModel()
    @State private var filter:String = ""
    @State var showCreateAdmin:Bool = false
    
    var body: some View {
        
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                    ButtonView(title:"Crear cuenta Administrador"){
                        showCreateAdmin = true
                    }.padding().sheet(isPresented:$showCreateAdmin, content:{
                        NewAdminView()
                        })
                    }
                    HStack(spacing:16){
                        InputTextFieldView(text: $filter, placeholder: "Nombre y apellidos", keyboardType: .namePhonePad, sfSymbol: "magnifyingglass")
                        Button(action: {
                            userDataViewModel.filterAllClients(filter:filter)
                        }, label: {
                            Text("Buscar")
                        })
                    }.padding()
                    if (userDataViewModel.messageError != ""){
                        Text(userDataViewModel.messageError).padding().foregroundColor(.red)
                    }
                    ForEach(userDataViewModel.users, id:\.id){ user in
                        NavigationLink(destination:AdminClientView(idUser:user.id!, name:user.name, surname:user.surname, phone:user.phone, email: user.email, activo:user.activo, role:user.role)){
                           HStack{
                               Text("\(user.surname), \(user.name)")
                               .font(.title)
                               .fontWeight(.semibold)
                               .foregroundColor(.black)
                               .padding([.leading], 10)
                               Spacer()
                           }
                       }
                       Spacer()
                    }
                }
            }.navigationViewStyle(StackNavigationViewStyle())
            .onAppear{
                userDataViewModel.getAllAdmins()
        }
    }
}


struct AdminsView_Previews: PreviewProvider {
    static var previews: some View {
        AdminsView()
    }
}
