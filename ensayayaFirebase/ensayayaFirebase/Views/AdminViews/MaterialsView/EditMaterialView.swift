import SwiftUI

struct EditMaterialView: View {
    
    @StateObject var materialViewModel = MaterialViewModel()
    @State var idMaterial:String
    @State var name:String
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre del material", keyboardType: .namePhonePad, sfSymbol: "music.mic").onReceive(name.publisher.collect()) {
                        self.name = String($0.prefix(20))
                    }
                }
                ButtonView(title:"Editar material"){
                    materialViewModel.updateMaterial(idMaterial:idMaterial, name:name)
                }.disabled(name.isEmpty)
                if (materialViewModel.messageError != ""){
                    Text(materialViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Editar \(name)")
            .applyClose()
        }
    }
}

struct EditMaterialView_Previews: PreviewProvider {
    static var previews: some View {
        EditMaterialView(idMaterial: "1", name: "Bajo Ibanez")
    }
}
