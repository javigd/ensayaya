import SwiftUI

struct AdminMaterialView: View {
    
    @StateObject var materialViewModel = MaterialViewModel()
    @StateObject var incidenceViewModel = IncidenceViewModel()
    @State var idMaterial:String
    @State var name:String
    @State var active:Bool
    @State private var isShowingConfirmation:Bool = false
    @State private var showEditMaterial:Bool = false
    @State private var showCreateIncidence:Bool = false
    
    var body: some View {
        
        VStack(spacing:16){
            Text("Nombre del material: \(name)").padding().font(.title)
            ScrollView{
                VStack(spacing:16){
                    ForEach(incidenceViewModel.incidences, id: \.id){ incidence in
                        Text("Incidencia: \(incidence.description)")
                        Text("Fecha de notificación \(incidence.notificationDate, style: .date)")
                        if (incidence.solutionDate == nil){
                            ButtonView(title:"Solucionar incidencia"){
                                incidenceViewModel.resolveIncidence(idIncidence:incidence.id ?? "")
                            }.padding()
                        }
                        else{
                            Text("Fecha de resolución \(incidence.solutionDate!, style: .date)")
                        }
                        Divider()
                    }
                }
            }
            if(incidenceViewModel.messageError != ""){
                Text(incidenceViewModel.messageError).padding().foregroundColor(.red)
            }
            if (materialViewModel.messageError != ""){
                Text(materialViewModel.messageError).padding().foregroundColor(.red)
            }
            if(active == false){
                ButtonView(title:"Activar Material"){
                    materialViewModel.activateMaterial(idMaterial:idMaterial)
                }.padding()
            }else{
                ButtonView(title:"Desactivar Material"){
                    materialViewModel.desactivateMaterial(idMaterial:idMaterial)
                }.padding()
            }
            ButtonView(title:"Crear Incidencia"){
                showCreateIncidence.toggle()
            }.padding().sheet(isPresented:$showCreateIncidence, content:{
                CreateIncidenceView(idObject:idMaterial, description: "")
            })
            ButtonView(title:"Editar Material"){
                showEditMaterial.toggle()
            }.padding().sheet(isPresented:$showEditMaterial, content:{
                EditMaterialView(idMaterial:idMaterial, name:name)
            })
            ButtonView(title:"Eliminar Material"){
                isShowingConfirmation = true
            }
            .padding()
            .confirmationDialog("¿Estás seguro de eliminar \(name)", isPresented:$isShowingConfirmation, titleVisibility: .visible){
                Button("Confirmar", role:.destructive){
                    materialViewModel.deleteMaterial(idMaterial: idMaterial)
                }
                Button("Cancelar", role:.cancel){}
            }
        }.onAppear{
            incidenceViewModel.getIncidenceById(id: idMaterial)
        }
    }
}


struct AdminMaterialView_Previews: PreviewProvider {
    static var previews: some View {
        AdminMaterialView(idMaterial: "1", name: "Bajo Ibanez", active: true)
    }
}
