import SwiftUI

struct CreateMaterialView: View {
    
    @State var name:String = ""
    @State var incidence:String = ""
    @State var active:Bool = false
    @StateObject var materialViewModel = MaterialViewModel()
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre del material", keyboardType: .namePhonePad, sfSymbol: "music.mic").onReceive(name.publisher.collect()) {
                        self.name = String($0.prefix(20))
                    }
                    Toggle("¿El material estará disponible?", isOn: $active)
                }
                ButtonView(title:"Crear material"){
                    materialViewModel.createMaterial(name:name,  active:active)
                }.disabled(name.isEmpty)
                if (materialViewModel.messageError != ""){
                    Text(materialViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Crear sala")
            .applyClose()
        }
    }
}

struct CreateMaterialView_Previews: PreviewProvider {
    static var previews: some View {
        CreateMaterialView()
    }
}
