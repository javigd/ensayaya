import SwiftUI

struct MaterialsView: View {
    
    @StateObject var materialViewModel = MaterialViewModel()
    @State var showCreateMaterial:Bool = false
    @State private var filter:String = ""
    
    var body: some View {
        
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                    ButtonView(title:"Crear material"){
                        showCreateMaterial.toggle()
                    }.padding().sheet(isPresented:$showCreateMaterial, content:{
                        CreateMaterialView()
                    })
                    HStack(spacing:16){
                        InputTextFieldView(text: $filter, placeholder: "Nombre del material", keyboardType: .namePhonePad, sfSymbol: "magnifyingglass")
                        Button(action: {
                            materialViewModel.filterMaterial(filter:filter)
                        }, label: {
                            Text("Buscar")
                        })
                    }.padding()
                    if (materialViewModel.messageError != ""){
                        Text(materialViewModel.messageError).padding().foregroundColor(.red)
                    }
                    ForEach(materialViewModel.materials, id:\.id){ material in
                        NavigationLink(destination:AdminMaterialView(idMaterial:material.id ?? "", name:material.name, active:material.active)){
                               Text(material.name)
                                   .font(.title)
                                   .fontWeight(.semibold)
                                   .multilineTextAlignment(.center)
                                   .foregroundColor(.black)
                                   .padding([.leading], 10)
                            Spacer()
                       }
                       Spacer()
                   }
                }.onAppear{
                    materialViewModel.getAllMaterials()
               }
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MaterialsView_Previews: PreviewProvider {
    static var previews: some View {
        MaterialsView()
    }
}
