import SwiftUI

struct CreateIncidenceView: View {
    
    @State var idObject:String
    @State var description:String
    @StateObject var incidenceViewModel = IncidenceViewModel()
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                Spacer()
                
                InputTextFieldView(text: $description, placeholder: "Indique el motivo de la incidencia", keyboardType: .namePhonePad, sfSymbol: "hammer").onReceive(description.publisher.collect()) {
                    self.description = String($0.prefix(75))
                }
                
                ButtonView(title:"Crear Incidencia"){
                    incidenceViewModel.createIncidence(id: idObject, description: description)
                }
                if (incidenceViewModel.messageError != ""){
                    Text(incidenceViewModel.messageError).padding().foregroundColor(.red)
                }
                Spacer()
            }.padding(.horizontal, 15)
                .navigationTitle("Crear incidencia")
                .applyClose()
        }
    }
}

struct CreateIncidenceView_Previews: PreviewProvider {
    static var previews: some View {
        CreateIncidenceView(idObject: "1", description: "No hay")
    }
}
