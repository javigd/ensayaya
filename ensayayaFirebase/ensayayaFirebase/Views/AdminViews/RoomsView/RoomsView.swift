import SwiftUI

struct RoomsView: View {
    
    @StateObject var roomViewModel = RoomViewModel()
    @State var showCreateRoom:Bool = false
    @State private var filter:String = ""
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                    ButtonView(title:"Crear sala"){
                        showCreateRoom.toggle()
                    }.padding().sheet(isPresented:$showCreateRoom, content:{
                        CreateRoomView()
                    })
                    HStack(spacing:16){
                        InputTextFieldView(text: $filter, placeholder: "Nombre de la sala", keyboardType: .namePhonePad, sfSymbol: "magnifyingglass")
                        Button(action: {
                            roomViewModel.filterRooms(filter:filter)
                        }, label: {
                            Text("Buscar")
                        })
                    }.padding()
                    if(roomViewModel.messageError != ""){
                        Text(roomViewModel.messageError).padding().foregroundColor(.red)
                    }
                    ForEach(roomViewModel.rooms, id:\.id){ room in
                       NavigationLink(destination:AdminRoomView(idRoom:room.id ?? "", title:room.title, description:room.description, active:room.active)){
                               Text(room.title)
                                   .font(.title)
                                   .fontWeight(.semibold)
                                   .multilineTextAlignment(.center)
                                   .foregroundColor(.black)
                                   .padding([.leading], 10)
                               Spacer()
                       }
                       Spacer()
                   }
                }.onAppear{
                    roomViewModel.getAllRooms()
               }
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct RoomsView_Previews: PreviewProvider {
    static var previews: some View {
        RoomsView()
    }
}
