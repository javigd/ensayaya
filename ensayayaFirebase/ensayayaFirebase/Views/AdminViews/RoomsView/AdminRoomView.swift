import SwiftUI

struct AdminRoomView: View {
    
    @StateObject var roomViewModel = RoomViewModel()
    @StateObject var incidenceViewModel = IncidenceViewModel()
    @State var idRoom:String
    @State var title:String
    @State var description:String
    @State var active:Bool
    @State private var isShowingConfirmation:Bool = false
    @State private var showEditRoom:Bool = false
    @State private var showCreateIncidence:Bool = false
    @State private var showBookings:Bool = false
    
    var body: some View {
        VStack(spacing:16){
            Text("Nombre de la sala: \(title)").padding().font(.title)
            Text("Descripción: \(description)").padding().font(.body)
            ScrollView{
                VStack(spacing:16){
                    ForEach(incidenceViewModel.incidences, id: \.id){ incidence in
                        Text("Incidencia: \(incidence.description)")
                        Text("Fecha de notificación \(incidence.notificationDate, style: .date)")
                        if (incidence.solutionDate == nil){
                            ButtonView(title:"Solucionar incidencia"){
                                incidenceViewModel.resolveIncidence(idIncidence:incidence.id ?? "")
                            }.padding()
                        }
                        else{
                            Text("Fecha de resolución \(incidence.solutionDate!, style: .date)")
                        }
                        Divider()
                    }
                }
            }
            if(roomViewModel.messageError != ""){
                Text(roomViewModel.messageError).padding().foregroundColor(.red)
            }
            if(incidenceViewModel.messageError != ""){
                Text(incidenceViewModel.messageError).padding().foregroundColor(.red)
            }
            if(active == false){
                ButtonView(title:"Activar Sala"){
                    roomViewModel.activateRoom(idRoom:idRoom)
                }.padding()
            }else{
                ButtonView(title:"Desactivar Sala"){
                    roomViewModel.desactivateRoom(idRoom:idRoom)
                }.padding()
            }
            ButtonView(title:"Ver Reservas"){
                showBookings.toggle()
            }.padding().sheet(isPresented:$showBookings, content:{
                RoomBookingsView(idRoom:idRoom, roomName:title)
            })
            ButtonView(title:"Crear Incidencia"){
                showCreateIncidence.toggle()
            }.padding().sheet(isPresented:$showCreateIncidence, content:{
                CreateIncidenceView(idObject:idRoom, description: "")
            })
            ButtonView(title:"Editar Sala"){
                showEditRoom.toggle()
            }.padding().sheet(isPresented:$showEditRoom, content:{
                EditRoomView(idRoom:idRoom, title:title, description:description)
            })
            ButtonView(title:"Eliminar Sala"){
                isShowingConfirmation = true
            }
            .padding()
            .confirmationDialog("¿Estás seguro de eliminar la \(title)", isPresented:$isShowingConfirmation, titleVisibility: .visible){
                Button("Confirmar", role:.destructive){
                    roomViewModel.deleteRoom(idRoom: idRoom)
                }
                Button("Cancelar", role:.cancel){}
            }
        }.onAppear{
            incidenceViewModel.getIncidenceById(id: idRoom)
        }
    }
}

struct AdminRoomView_Previews: PreviewProvider {
    static var previews: some View {
        AdminRoomView(idRoom: "1", title:"Sala 1", description:"Sala muy bien equipada", active:true)
    }
}
