import SwiftUI

struct CreateRoomView: View {
    
    @State var title:String = ""
    @State var description:String = ""
    @State var active:Bool = false
    @StateObject var roomViewModel = RoomViewModel()
    
    var body: some View {
        
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $title, placeholder: "Nombre de la sala", keyboardType: .namePhonePad, sfSymbol: "music.note.house").onReceive(title.publisher.collect()) {
                        self.title = String($0.prefix(20))
                    }
                    InputTextFieldView(text: $description, placeholder: "Descripción", keyboardType: .namePhonePad, sfSymbol: "rectangle.and.pencil.and.ellipsis").onReceive(description.publisher.collect()) {
                        self.description = String($0.prefix(75))
                    }
                    Toggle("¿La sala estará disponible?", isOn: $active)
                }
                ButtonView(title:"Crear sala"){
                    roomViewModel.createRoom(title:title, description:description, active:active)
                }.disabled(title.isEmpty || description.isEmpty)
                if(roomViewModel.messageError != ""){
                    Text(roomViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Crear sala")
            .applyClose()
        }
    }
}

struct CreateRoomView_Previews: PreviewProvider {
    static var previews: some View {
        CreateRoomView()
    }
}
