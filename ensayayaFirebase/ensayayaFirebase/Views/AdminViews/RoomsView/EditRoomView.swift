import SwiftUI

struct EditRoomView: View {
    
    @StateObject var roomViewModel = RoomViewModel()
    @State var idRoom:String
    @State var title:String
    @State var description:String
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $title, placeholder: "Nombre de la sala", keyboardType: .namePhonePad, sfSymbol: "music.note.house").onReceive(title.publisher.collect()) {
                        self.title = String($0.prefix(20))
                    }
                    InputTextFieldView(text: $description, placeholder: "Descripción", keyboardType: .namePhonePad, sfSymbol: "rectangle.and.pencil.and.ellipsis").onReceive(description.publisher.collect()) {
                        self.description = String($0.prefix(75))
                    }
                }
                ButtonView(title:"Editar sala"){
                    roomViewModel.updateRoom(idRoom:idRoom, title:title, description:description)
                }.disabled(title.isEmpty || description.isEmpty)
                if(roomViewModel.messageError != ""){
                    Text(roomViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Editar \(title)")
            .applyClose()
        }
    }
}

struct EditRoomView_Previews: PreviewProvider {
    static var previews: some View {
        EditRoomView(idRoom: "1", title: "Sala 1", description: "Sala muy bien equipada")
    }
}
