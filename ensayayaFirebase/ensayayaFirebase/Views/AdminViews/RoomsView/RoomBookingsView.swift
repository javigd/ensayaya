import SwiftUI

struct RoomBookingsView: View {
    
    @StateObject var bookingViewModel = BookingViewModel()
    @State var idRoom:String
    @State var roomName:String
    
    var body: some View {
        NavigationView{
            VStack(spacing:16){
                ForEach(bookingViewModel.bookings, id:\.id){ booking in
                    Text("\(booking.fullName) - \(booking.FechaReserva, style:.date) \(booking.horaInicio):00 - \(booking.horaFin):00")
                }
                Spacer()
            }.onAppear{
                bookingViewModel.roomBookings(idRoom:idRoom)
            }
        }.navigationTitle("Reservas realizadas en la \(roomName)")
    }
}

struct RoomBookingsView_Previews: PreviewProvider {
    static var previews: some View {
        RoomBookingsView(idRoom:"1", roomName:"Sala 1")
    }
}
