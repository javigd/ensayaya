import SwiftUI
import Combine //para usar el Just

struct EditVoucherView: View {
    
    @State var idVoucher:String
    @State var name:String
    @State var usages:String
    @State var price:String
    @ObservedObject var input = NumbersOnly()
    @StateObject var voucherViewModel = VoucherViewModel()
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre del bono", keyboardType: .namePhonePad, sfSymbol: "banknote").onReceive(name.publisher.collect()) {
                        self.name = String($0.prefix(20))
                    }
                    TextField("Número de usos", text:$usages).keyboardType(.decimalPad)
                        .onReceive(Just(usages)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.usages = filtered
                            }
                    }
                    TextField("Número de usos", text:$price).keyboardType(.decimalPad)
                        .onReceive(Just(price)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.price = filtered
                            }
                    }
                }
                ButtonView(title:"Editar bono"){
                    voucherViewModel.updateVoucher(idVoucher:idVoucher, usages:usages ,name:name, price:price)
                }.disabled(name.isEmpty)
                if (voucherViewModel.messageError != ""){
                    Text(voucherViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Editar \(name)")
            .applyClose()
        }
    }
}

struct EditVoucherView_Previews: PreviewProvider {
    static var previews: some View {
        EditVoucherView(idVoucher:"1", name:"Hola", usages:"2", price:"60.00")
    }
}
