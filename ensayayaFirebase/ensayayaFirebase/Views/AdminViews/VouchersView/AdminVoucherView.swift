import SwiftUI

struct AdminVoucherView: View {
    
    @StateObject var voucherViewModel = VoucherViewModel()
    @State var showEditVoucher:Bool = false
    @State var isShowingConfirmation:Bool = false
    @State var idVoucher:String
    @State var name:String
    @State var usages:Int
    @State var price:Double
    
    var body: some View {
        VStack(spacing:16){
            Text("Nombre del bono: \(name)").padding().font(.title)
            Text("Número de usos: \(usages)").padding().font(.body)
            Text("Precio: \(price, specifier: "%.2f")€").padding().font(.body)
            if (voucherViewModel.messageError != ""){
                Text(voucherViewModel.messageError).padding().foregroundColor(.red)
            }
            ButtonView(title:"Editar Bono"){
                showEditVoucher.toggle()
            }.padding().sheet(isPresented:$showEditVoucher, content:{
                EditVoucherView(idVoucher:idVoucher, name:name, usages:String(usages), price:String(price))
            })
            ButtonView(title:"Eliminar Bono"){
                isShowingConfirmation = true
            }
            .padding()
            .confirmationDialog("¿Estás seguro de eliminar el \(name)", isPresented:$isShowingConfirmation, titleVisibility: .visible){
                Button("Confirmar", role:.destructive){
                    voucherViewModel.deleteVoucher(idVoucher: idVoucher)
                }
                Button("Cancelar", role:.cancel){}
            }
        }
    }
}

struct AdminVoucherView_Previews: PreviewProvider {
    static var previews: some View {
        AdminVoucherView(idVoucher: "1", name: "Bono 100", usages: 100, price: 1000.00)
    }
}
