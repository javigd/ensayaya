import SwiftUI

struct VouchersView: View {
    
    @StateObject var voucherViewModel = VoucherViewModel()
    @State var showCreateVoucher:Bool = false
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                    ButtonView(title:"Crear bono"){
                        showCreateVoucher.toggle()
                    }.padding().sheet(isPresented:$showCreateVoucher, content:{
                        CreateVoucherView()
                    })
                    if (voucherViewModel.messageError != ""){
                        Text(voucherViewModel.messageError).padding().foregroundColor(.red)
                    }
                    ForEach(voucherViewModel.vouchers, id:\.id){ voucher in
                        NavigationLink(destination:AdminVoucherView(idVoucher:voucher.id ?? "", name:voucher.name, usages:voucher.usages, price:voucher.price)){
                           HStack{
                               Text(voucher.name)
                                   .font(.title)
                                   .fontWeight(.semibold)
                                   .multilineTextAlignment(.center)
                                   .foregroundColor(.black)
                               Spacer()
                           }.padding()
                       }
                       Spacer()
                   }
                }.onAppear{
                    voucherViewModel.getAllVouchers()
               }
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct VouchersView_Previews: PreviewProvider {
    static var previews: some View {
        VouchersView()
    }
}
