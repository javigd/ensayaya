import SwiftUI
import Combine

struct CreateVoucherView: View {
    
    private var mobReque:Array<Bool>{
        [
           
        ]
    }
    
    @StateObject var voucherViewModel = VoucherViewModel()
    @State var name:String = ""
    @State var usages:String = "0"
    @State var price:String = "0.00"
    
    private static let formatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            return formatter
        }()
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre del bono", keyboardType: .namePhonePad, sfSymbol: "banknote").onReceive(name.publisher.collect()) {
                        self.name = String($0.prefix(20))
                    }
                    TextField("Número de usos", text:$usages).keyboardType(.decimalPad)
                        .onReceive(usages.publisher.collect()) {
                                self.usages = String($0.prefix(2))
                            }
                        .onReceive(Just(usages)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.usages = filtered
                            }
                    }
                    TextField("Precio del bono", text:$price).keyboardType(.decimalPad)
                        .onReceive(price.publisher.collect()) {
                                self.price = String($0.prefix(6))
                            }
                        .onReceive(Just(price)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.price = filtered
                            }
                    }
                }
                ButtonView(title:"Crear bono"){
                    voucherViewModel.createVoucher(usages:usages ,price:price, name:name)
                }.disabled(name.isEmpty)
                if (voucherViewModel.messageError != ""){
                    Text(voucherViewModel.messageError).padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Crear bono")
            .applyClose()
        }
    }
}

struct CreateVoucherView_Previews: PreviewProvider {
    static var previews: some View {
        CreateVoucherView()
    }
}
