//AÑADIR NOMBRES DE USUARIO EN SESIÓN

import SwiftUI

struct SideMenuHeaderView: View {
    
    @Binding var isShowingMenu:Bool //Recibimos el valor de isShowingMenu de la otra vista
    
    var body: some View {
        ZStack(alignment: .topTrailing){
            Button(action: {
                withAnimation(.spring()){
                    isShowingMenu.toggle()
                }
            },  label:{
                Image(systemName: "xmark").frame(width:44, height:44)
            })
            VStack(alignment: .leading){
                Image(systemName:"person.circle.fill").resizable().scaledToFill().clipped().frame(width: 64, height: 64).clipShape(Circle()).padding(.bottom, 16)
                
                Text(UserDefaults.standard.string(forKey: "username") ?? "").font(.system(size: 24, weight: .semibold)).padding(.bottom, 32)
                
                HStack(spacing:12){
                    HStack(spacing:4){
                        Image(systemName: "guitars.fill").resizable().scaledToFill().frame(width: 35, height:35)
                    }
                    HStack(spacing:4){
                        Image(systemName: "music.mic").resizable().scaledToFill().frame(width: 35, height:35)
                    }
                    HStack(spacing:4){
                        Image(systemName: "pianokeys.inverse").resizable().scaledToFill().frame(width: 35, height:35)
                    }
                    Spacer()
                }

            }.padding()
        }
        
    }
}

struct SideMenuHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        SideMenuHeaderView(isShowingMenu: .constant(true))
    }
}
