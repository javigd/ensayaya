import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    @State private var isShowingMenu = false
    
    var body: some View {
        
        NavigationView{
            ZStack{
                if isShowingMenu{
                    SideMenuView(isShowingMenu: $isShowingMenu)
                }
                
                InfoView()
                    .cornerRadius(isShowingMenu ? 20 : 10)
                    .offset(x:isShowingMenu ? 300 : 0, y: isShowingMenu ? 44 : 0)
                    .scaleEffect(isShowingMenu ? 0.8:1)
                    .navigationBarItems(leading:Button(action: {
                        withAnimation(.spring()){
                            isShowingMenu.toggle()
                        }
                }, label: {Image(systemName: "list.bullet").foregroundColor(Color.black)
                }))
                    .navigationTitle("Ensayaya")
                    .navigationBarTitleDisplayMode(.inline)
            }/*.onAppear{
                isShowingMenu = false
            }*/ //El onApper nos sirve si queremos que el Menú aparezca o no cuando volvamos hacia atrás desde una Navegation View
             //Para situar el navigationTitle en la parte superior
        }.navigationViewStyle(StackNavigationViewStyle()) //Esta línea nos sirve para que los navigationView funcionen correctamente en iPad
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

