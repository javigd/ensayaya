import SwiftUI

struct InfoView: View {
    
    @State private var showListRoom:Bool = false
    @State private var showListVoucher:Bool = false
    @State private var showListMaterial:Bool = false
    
    var body: some View {
        ZStack{
            Color(.white)
            ScrollView{
                
            VStack(spacing:16){
                
                Image("logo").resizable().scaledToFit()
                Text("Ensayaya, tus salas de ensayo al mejor precio y con la mayor calidad")
                    .font(.title)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .padding(.top, 10)
                ScrollView{
                    Image("default_rehearsal_room").resizable().scaledToFit().padding()
                    ButtonView(title:"Nuestras Salas"){
                        showListRoom.toggle()
                    }.padding().sheet(isPresented:$showListRoom, content:{
                        ListRoomView()
                    })
                    Image("default_material").resizable().scaledToFit().padding()
                    ButtonView(title:"Nuestros Instrumentos"){
                        showListMaterial.toggle()
                    }.padding().sheet(isPresented:$showListMaterial, content:{
                        ListMaterialView()
                    })
                    Image("post_it").resizable().scaledToFit().padding()
                    ButtonView(title:"Nuestros Bonos"){
                        showListVoucher.toggle()
                    }.padding().sheet(isPresented:$showListVoucher, content:{
                        ListVoucherView()
                    })
                }
                }.padding(.horizontal,15)
            }
        }
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}

