import SwiftUI

struct SignInView: View {
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel
    @State var email = ""
    @State var password = ""
    @State private var showRegistration = false //Variables que son modificadas dinámicamente en la vista --> Propiedad @State (su cambio no es propagado a vistas diferentes)
    @State private var showRememberPassword = false
    @State var showProgressView = false
    
    var body: some View {
        
        VStack(spacing:16){
            
            Image("logo").resizable().scaledToFit()
            
            VStack(spacing:16){
                InputTextFieldView(text: $email, placeholder: "Email", keyboardType: .namePhonePad, sfSymbol: "person.fill")
                InputPasswordView(password: $password, placeholder: "Contraseña", sfSymbol: "lock")
            }
            HStack{
                Spacer()
                Button(action:{
                    showRememberPassword.toggle() //Un toggle nos permite cambiar el valor de un booleano automáticamente
                }, label:{
                    Text("¿Olvidaste tu contraseña?")
                })
                    .font(.system(size:16, weight:.bold))
                    .sheet(isPresented:$showRememberPassword, content:{
                        RememberPasswordView()//Al cambiar la variable a true, nos muestra el modal o sheet.
                    })
            }
            ButtonView(title: "Iniciar Sesión"){
                authenticationViewModel.login(email: email, password: password)
                showProgressView = true
            }.disabled(email.isEmpty || password.isEmpty)
            if(showProgressView == true && authenticationViewModel.messageLogin == ""){
                ProgressView() //Vista que proporciona swiftui para mostrar un spinner de carga
            }
            ButtonView(title: "Registrarse", background: .white, foreground: .black, border: .black){
                showRegistration.toggle() //Al cambiar la variable a true, nos muestra el modal o sheet.
            }.sheet(isPresented:$showRegistration, content:{
                RegisterView()
            })
            if let messageLogin = authenticationViewModel.messageLogin{
                Text(messageLogin).bold().font(.body).foregroundColor(.red).padding(.top,20)
            }
        }
        .padding(.horizontal, 15)
        .navigationTitle("Iniciar Sesión")
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
