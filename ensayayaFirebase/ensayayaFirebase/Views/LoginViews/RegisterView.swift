import SwiftUI
import Combine

struct RegisterView: View {
    
    private var requirements: Array<Bool> {
        [
            password.count >= 7,             // Longitud mínima
            password &= "[0-9]",             // Al menos un número
            password &= ".*[^A-Za-z0-9].*"   // Al menos un caracter especial
        ]
    }
    private var mobReque:Array<Bool>{
        [
            phone.count == 9,
            String(phone.prefix(1)) &= "[6-7]"
        ]
    }
    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    @State private var password:String = ""
    @State private var confirmPassword:String = ""
    @State private var email:String = ""
    @State private var name:String = ""
    @State private var surname:String = ""
    @State private var phone:String = ""
    @State private var username:String = ""
    
    var body: some View {
        NavigationView{
            VStack(spacing:32){
                VStack(spacing:16){
                    InputTextFieldView(text: $name, placeholder: "Nombre", keyboardType: .emailAddress, sfSymbol: "person").onReceive(name.publisher.collect()) {
                        self.name = String($0.prefix(20))
                    }
                    InputTextFieldView(text: $surname, placeholder: "Apellidos", keyboardType: .emailAddress, sfSymbol: "person").onReceive(surname.publisher.collect()) {
                        self.surname = String($0.prefix(25))
                    }
                    InputTextFieldView(text: $phone, placeholder: "Número de teléfono", keyboardType: .phonePad, sfSymbol: "phone").onReceive(phone.publisher.collect()) {
                        self.phone = String($0.prefix(9))
                    }.onReceive(Just(phone)) { newValue in
                        let filtered = newValue.filter { "0123456789".contains($0) }
                        if filtered != newValue {
                            self.phone = filtered
                        }
                    }
                    InputTextFieldView(text: $email, placeholder: "Email", keyboardType: .namePhonePad, sfSymbol: "envelope").onReceive(email.publisher.collect()) {
                        self.email = String($0.prefix(40))
                    }
                    InputTextFieldView(text: $username, placeholder: "Nombre de usuario", keyboardType: .namePhonePad, sfSymbol: "envelope").onReceive(username.publisher.collect()) {
                        self.username = String($0.prefix(15))
                    }
                    InputPasswordView(password: $password, placeholder: "Contraseña", sfSymbol: "lock").onReceive(password.publisher.collect()) {
                        self.password = String($0.prefix(20))
                    }
                    Group{
                        Text("Mínimo 7 caracteres")
                            .foregroundColor(self.requirements[0] ? .green : .red)
                        Text("Al menos 1 número")
                            .foregroundColor(self.requirements[1] ? .green : .red)
                        Text("1 caracter especial (! @ # $ % ^ * ?)")
                            .foregroundColor(self.requirements[2] ? .green : .red)
                        if (password != confirmPassword){
                            Text("Las contraseñas no coinciden").foregroundColor(.red).padding()
                        }
                    }
                    .padding()
                    InputPasswordView(password: $confirmPassword, placeholder: "Confirmar Contraseña", sfSymbol: "lock").onReceive(confirmPassword.publisher.collect()) {
                        self.confirmPassword = String($0.prefix(20))
                    }
                }
                
                ButtonView(title:"Registrarse"){
                    authenticationViewModel.createNewUser(name:name, surname:surname, phone:phone, email: email, password: password, username:username)
                }.disabled(password != confirmPassword || name.isEmpty || surname.isEmpty || phone.isEmpty || email.isEmpty || username.isEmpty || self.requirements[0] == false || self.requirements[1] == false || self.requirements[2] == false || self.mobReque[0] == false || self.mobReque[1] == false)
                if let messageError = authenticationViewModel.messageError {
                    Text(messageError).bold().font(.body).foregroundColor(.red).padding(.top, 20)
                }
            }.padding(.horizontal, 15)
            .navigationTitle("Nueva Cuenta")
            .applyClose()
        }
    }
}

extension View { //Extensión de la vista para darle algo de formato a campos que no utilizan ninguna de las UtilViews. Es como un método.
    public func datePickerField() -> some View {
        return self
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .background(Color.white)
            .frame(height: 44)
            .cornerRadius(10)
    }
}

extension String {
    static func &= (lhs: String, rhs: String) -> Bool {
        return lhs.range(of: rhs,options: .regularExpression) != nil
    }
}


struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
