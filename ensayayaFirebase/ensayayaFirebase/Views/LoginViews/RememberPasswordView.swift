
import SwiftUI

struct RememberPasswordView: View {
    
    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    @State var email = ""
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        
        NavigationView{
            
            VStack(spacing:16){
                
                InputTextFieldView(text: $email, placeholder: "Email", keyboardType: .emailAddress, sfSymbol: "envelope")
                
                ButtonView(title:"Enviar email"){
                    authenticationViewModel.rememberPassword(email: email)
                    presentationMode.wrappedValue.dismiss()
                }
                if (authenticationViewModel.errorPassword != ""){
                    Text(authenticationViewModel.errorPassword ?? "").padding().foregroundColor(.red)
                }
            }.padding(.horizontal, 15)
                .navigationTitle("Recordar Contraseña")
                .applyClose()
        }
    }
}

struct RememberPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        RememberPasswordView()
    }
}

