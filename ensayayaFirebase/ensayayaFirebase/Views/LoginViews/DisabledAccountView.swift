import SwiftUI

//ESTO HAY QUE HACERLO TODAVÍA

struct DisabledAccountView: View {
    
    @State var showSignIn:Bool = false
    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    
    
    var body: some View {
        
        if (showSignIn == false){
            VStack(spacing:16){
                Spacer()
                Text("Su cuenta ha sido desactivada").font(.title2).padding()
                Text("Por favor, contacte con un administrador").font(.body).padding()
                ButtonView(title: "Volver al Inicio de Sesión"){
                    showSignIn.toggle()
                    authenticationViewModel.logout()
                }.padding()
                Spacer()
                Image("logo").resizable().scaledToFit()
                Spacer()
            }
        }else{
            SignInView()
        }
    }
}

struct DisabledAccountView_Previews: PreviewProvider {
    static var previews: some View {
        DisabledAccountView()
    }
}
