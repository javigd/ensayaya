import SwiftUI

struct VerificateEmailView: View {

    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    @State var showSignIn:Bool = false
    
    var body: some View {
        if showSignIn == false{
            VStack(spacing:16){
                Spacer()
                Text("Por favor, verifique su dirección de email mediante el correo electrónico enviado. Una vez verificado, podrá iniciar sesión con normalidad").font(.body).fontWeight(.bold).multilineTextAlignment(.center).padding()
                ButtonView(title:"Volver a Iniciar Sesión"){
                    showSignIn.toggle()
                    authenticationViewModel.logout()
                }.padding()
                
                Spacer()
            }
        }else{
            SignInView()
        }
    }
}

struct VerificateEmailView_Previews: PreviewProvider {
    static var previews: some View {
        VerificateEmailView()
    }
}
