import SwiftUI

struct MyAccountView: View {
    
    @StateObject var voucherViewModel = VoucherViewModel()
    @StateObject var authenticationViewModel = AuthenticationViewModel()
    @StateObject var bookingViewModel = BookingViewModel()
    @State var isShowingConfirmation:Bool = false
    @State var deleteAccount:Bool = false
    let date:Date = Date()
    
    var body: some View {
        
        NavigationView{
            VStack(alignment: .leading, spacing: 32){
                
                Text("Nombre y apellidos: \(UserDefaults.standard.string(forKey: "name") ?? "") \(UserDefaults.standard.string(forKey:"surname") ?? "")")
                Text("Teléfono de contacto: \(UserDefaults.standard.string(forKey: "phone") ?? "")")
                Text("Email de contacto:\(UserDefaults.standard.string(forKey:"email") ?? "")")
                NavigationLink(destination: UpdateUserAccountView(), label:{
                    ImpostorButton(title:"Editar Cuenta")
                })
                if (UserDefaults.standard.string(forKey:"role") ?? "" == "cliente"){
                    Text("Mis bonos activos").font(.title3)
                    ForEach(voucherViewModel.uVoucher, id:\.id){ voucher in
                        Text("Tipo de bono: \(voucher.name)")
                        Text("Número de usos restantes: \(voucher.numUsages)")
                        Text("Usos totales del bono: \(voucher.usages)")
                    }
                }
                if(UserDefaults.standard.string(forKey:"id") ?? "" != "jorGx5dzYlcsio78OEdslUrB7lx2"){
                    ButtonView(title:"Eliminar Cuenta"){
                       deleteAccount = true
                    }.confirmationDialog("¿Estás seguro de eliminar tu cuenta? Las reservas y bonos que tenga no se eliminarán", isPresented:$deleteAccount, titleVisibility: .visible){
                        Button("Confirmar", role:.destructive){
                            authenticationViewModel.deleteAccount(idUser: UserDefaults.standard.string(forKey:"id") ?? "")
                        }
                        Button("Cancelar", role:.cancel){}
                    }
                }
                if(UserDefaults.standard.string(forKey:"id") ?? "" == "jorGx5dzYlcsio78OEdslUrB7lx2"){
                    ButtonView(title:"Eliminar Reservas Antiguas"){
                        isShowingConfirmation = true
                    }.confirmationDialog("¿Estás seguro de eliminar las reservas antiguas?", isPresented:$isShowingConfirmation, titleVisibility: .visible){
                        Button("Confirmar", role:.destructive){
                            bookingViewModel.deleteOldBookings()
                        }
                        Button("Cancelar", role:.cancel){}
                    }
                    if(bookingViewModel.messageError != ""){
                        Text(bookingViewModel.messageError).padding()
                    }
                }
                Spacer()
                Image("logo").resizable().scaledToFit()
                Spacer()
            }.onAppear{
                voucherViewModel.voucherPerId(idUser: UserDefaults.standard.string(forKey:"id") ?? "")
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .padding()
        .navigationTitle("Mi Cuenta")
    }
}

struct MyAccountView_Previews: PreviewProvider {
    static var previews: some View {
        MyAccountView()
    }
}
