import SwiftUI
import Combine

struct UpdateUserAccountView: View {
    
    private var requirements: Array<Bool> {
        [
            password.count >= 7,             // Longitud mínima
            password &= "[0-9]",             // Al menos un número
            password &= ".*[^A-Za-z0-9].*"   // Al menos un caracter especial
        ]
    }
    
    private var mobReque:Array<Bool>{
        [
            phone.count == 9,
            String(phone.prefix(1)) &= "[6-7]"
        ]
    }
    
    @State var name:String = UserDefaults.standard.string(forKey:"name") ?? ""
    @State var surname: String = UserDefaults.standard.string(forKey:"surname") ?? ""
    @State var phone:String = UserDefaults.standard.string(forKey:"phone") ?? ""
    @State var email:String = UserDefaults.standard.string(forKey:"email") ?? ""
    @State var password:String = ""
    @EnvironmentObject var authenticationViewModel:AuthenticationViewModel
    @StateObject var userDataViewModel = UserDataViewModel()
    @State var confirmPassword:String = ""
    @State var changedPassword:Bool = false
    @State var changedUserData:Bool = false
    
    var body: some View {
        VStack(spacing:32){
            VStack(spacing:16){
                InputTextFieldView(text: $name, placeholder: "Nombre", keyboardType: .namePhonePad, sfSymbol: "person.fill").onReceive(name.publisher.collect()) {
                    self.name = String($0.prefix(20))
                }
                InputTextFieldView(text: $surname, placeholder: "Apellidos", keyboardType: .namePhonePad, sfSymbol: "person.fill").onReceive(surname.publisher.collect()) {
                    self.surname = String($0.prefix(25))
                }
                InputTextFieldView(text: $phone, placeholder: "Teléfono", keyboardType: .phonePad, sfSymbol: "phone").onReceive(phone.publisher.collect()) {
                    self.phone = String($0.prefix(9))
                }.onReceive(Just(phone)) { newValue in
                    let filtered = newValue.filter { "0123456789".contains($0) }
                    if filtered != newValue {
                        self.phone = filtered
                    }
                }
                InputTextFieldView(text: $email, placeholder: "Email", keyboardType: .namePhonePad, sfSymbol: "mail").onReceive(email.publisher.collect()) {
                    self.email = String($0.prefix(40))
                }
                ButtonView(title:"Editar Cuenta"){
                    changedUserData.toggle()
                    userDataViewModel.setNewUserData(name: name, surname: surname, phone: phone)
                    if(email != UserDefaults.standard.string(forKey:"email") ?? ""){
                        authenticationViewModel.changeEmail(email:email)
                    }
                }.disabled(name.isEmpty || surname.isEmpty || phone.isEmpty || email.isEmpty || self.mobReque[0] == false || self.mobReque[1] == false)
                if(changedUserData == true){
                    Text("Los datos han sido actualizados").foregroundColor(.red)
                }
                Divider()
                
                InputPasswordView(password:$password, placeholder:"Introduzca nueva contraseña", sfSymbol:"lock").onReceive(password.publisher.collect()) {
                    self.password = String($0.prefix(20))
                }
                
                InputPasswordView(password:$confirmPassword, placeholder:"Repita la nueva contraseña", sfSymbol:"lock").onReceive(confirmPassword.publisher.collect()) {
                    self.confirmPassword = String($0.prefix(20))
                }
                if (password != confirmPassword){
                    Text("Las contraseñas no coinciden y deben contener caracteres especiales y un número").foregroundColor(.red).padding()
                }
            }.onAppear{
                changedPassword = false
                }
            }.padding(.horizontal, 15)
        VStack(spacing:32){
            VStack(spacing:16){
                if (changedPassword == true){
                    Text("La contraseña ha sido actualizada")
                }
                ButtonView(title:"Editar Contraseña"){
                    authenticationViewModel.changePassword(password: password)
                }
                .disabled(password.isEmpty || confirmPassword.isEmpty || confirmPassword != password || self.requirements[0] == false || self.requirements[1] == false || self.requirements[2] == false)
            }
        }.padding(.horizontal, 15)
        if (authenticationViewModel.messageError != ""){
            Text(authenticationViewModel.messageError ?? "").padding().foregroundColor(.red)
        }
    }
}

struct UpdateUserAccountView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateUserAccountView()
    }
}
