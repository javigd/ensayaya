import SwiftUI

struct ListVoucherView: View {
    
    @StateObject var voucherModel = VoucherViewModel()
    @State var showConfirmation:Bool = false
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment:.leading, spacing:16){
                   ForEach(voucherModel.vouchers, id:\.id){ voucher in
                       Text(voucher.name)
                           .font(.title)
                           .fontWeight(.semibold)
                       Text("Usos:  \(voucher.usages)")
                       Text("Precio:  \(voucher.price, specifier: "%.2f")€")
                       if (UserDefaults.standard.string(forKey: "role") ?? "" == "cliente"){
                           ButtonView(title: "Comprar"){
                               voucherModel.buyVoucher(idUser: UserDefaults.standard.string(forKey:"id") ?? "" ,name:voucher.name, usages:voucher.usages)
                           }
                           //Esto no puedo hacerlo ya que carga el último en aparecer siempre, por lo que no
                           //puedo poner mensaje de confirmación
                           /*.confirmationDialog("¿Estás seguro de comprar el \(voucher.name)", isPresented:$showConfirmation, titleVisibility: .visible){
                               Button("Confirmar", role: .destructive){
                                   voucherModel.buyVoucher(idUser: UserDefaults.standard.string(forKey:"id") ?? "" ,name:voucher.name, usages:voucher.usages)
                               }
                               Button("Cancelar", role:.cancel){}
                           }*/
                       }
                   }
                    if(voucherModel.messageError != ""){
                        Text(voucherModel.messageError).padding().foregroundColor(.red)
                    }
                }
                .padding()
                .padding(.top,50)
                .onAppear{
                     voucherModel.getAllVouchers()
                }
                .navigationTitle("Lista de Bonos")
            }.applyClose()
        }
    }
}


struct ListVoucherView_Previews: PreviewProvider {
    static var previews: some View {
        ListVoucherView()
    }
}
