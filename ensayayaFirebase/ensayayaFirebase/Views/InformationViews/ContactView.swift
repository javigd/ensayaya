import SwiftUI

struct ContactView: View {
    var body: some View {
        
        VStack(spacing:16){
            Text("Información de contacto").font(.title).multilineTextAlignment(.center).padding()
            Divider()
            Text("Teléfono de contacto: 600 000 000").font(.body)
            Text("Email de contacto: info@javiergddw.com")
            Text("Dirección: C/ Sin nombre Nº5")
            Spacer()
            //Añadir aquí una ubicación de google maps para que quede más molongo
            //Quitar la imagen cuando sepa poner el google maps --> Mirar el map de UIKit
            Image("logo").resizable().scaledToFit()
            Spacer()
            
        }
    }
}

struct ContactView_Previews: PreviewProvider {
    static var previews: some View {
        ContactView()
    }
}
