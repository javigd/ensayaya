import SwiftUI

struct BookingWithVoucherView: View {
    
    @StateObject var voucherViewModel = VoucherViewModel()
    @Binding var finalHour:Int
    @Binding var hourSelected:Int
    @Binding var voucherSelected:String
    @Binding var voucherNumUsages:Int
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(spacing:16){
                    Text("Mis bonos activos")
                    ForEach(voucherViewModel.uVoucher, id:\.id){ voucher in
                        if(voucher.numUsages >= (finalHour - hourSelected)){
                            Button(action: {
                                voucherSelected = voucher.id!
                                voucherNumUsages = voucher.numUsages
                                }, label: {
                                    Text("\(voucher.name) - Usos Restantes: \(voucher.numUsages)")
                                        .padding()
                                        .foregroundColor(Color.black)
                                        .background(Color.gray)
                                        .cornerRadius(10)
                                        .shadow(radius: 10)
                            })
                        }
                    }
                    Spacer()
                }.onAppear{
                    voucherViewModel.voucherPerId(idUser: UserDefaults.standard.string(forKey: "id")!)
                }
            }.applyClose()
        }
    }
}
