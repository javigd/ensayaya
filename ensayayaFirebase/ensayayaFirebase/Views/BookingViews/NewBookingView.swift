import SwiftUI

struct NewBookingView: View {
    
    @State var currentDate:Date = Date()
    @State var roomSelected:String = ""
    @State var hourSelected:Int = 0
    @State var finalHour:Int = 0
    @State var voucherSelected:String = ""
    @State var showVoucher:Bool = false
    @State var voucherNumUsages:Int = 0
    @State var showHours:Bool = false
    @State var showFinalHour:Bool = false
    @StateObject var roomViewModel = RoomViewModel()
    @StateObject var bookingViewModel = BookingViewModel()
    @StateObject var voucherViewModel = VoucherViewModel()
    //Para poner el DatePicker en Español
    let dateFormatter = DateFormatter()
    //dateFormatter.locale = Locale(identifier: "es_ES") es algo así para ponerlo en español
    // y se tiene que poner esto tambien en el datePicker --> .environment(\.locale, Locale.init(identifier: "es_ES")
    var body: some View {
            VStack(spacing:16){
                Form{
                    if (roomSelected == ""){
                        DatePicker("Fecha de la reserva", selection: $currentDate, in: Date()..., displayedComponents: .date)
                        //Text(currentDate, style: .date)
                        ForEach(roomViewModel.rooms, id:\.id){ room in
                            Button(action: {
                                roomSelected = room.id!
                                }, label: {
                                    Text(room.title)
                                        .padding()
                                        .foregroundColor(Color.black)
                                        .background(Color.gray)
                                        .cornerRadius(10)
                                        .shadow(radius: 10)
                            })
                        }
                    }
                    if (roomSelected != ""){
                        if (finalHour == 0){
                            ButtonView(title:"Seleccionar hora Inicial"){
                                showHours.toggle()
                            }.padding().sheet(isPresented:$showHours, content:{
                                BookingHoursView(roomSelected: $roomSelected, currentDate: $currentDate, hourSelected: $hourSelected)
                            })
                        }
                        if (hourSelected != 0){
                            ButtonView(title:"Seleccionar hora Fin"){
                                showFinalHour.toggle()
                            }.padding().sheet(isPresented:$showFinalHour, content:{
                                BookingFinalHoursView(roomSelected: $roomSelected, currentDate: $currentDate, hourSelected: hourSelected, finalHour: $finalHour)
                            })
                            if (finalHour != 0){
                                    ButtonView(title:"Usar bono activo"){
                                        showVoucher.toggle()
                                    }.padding().sheet(isPresented:$showVoucher, content:{
                                        BookingWithVoucherView(finalHour: $finalHour, hourSelected: $hourSelected, voucherSelected: $voucherSelected, voucherNumUsages: $voucherNumUsages)
                                    })
                                VStack{
                                    Text("Sala \(roomSelected) - \(currentDate, style:.date)")
                                    Text("Hora Inicio: \(hourSelected):00 Hora Fin: \(finalHour):00")
                                }
                                ButtonView(title:"Reservar Sala"){
                                    bookingViewModel.booking(date:currentDate, idRoom:roomSelected, horaInicio:hourSelected, horaFin:finalHour, bono:voucherSelected, usosBono:voucherNumUsages)
                                    hourSelected = 0
                                    roomSelected = ""
                                    finalHour = 0
                                    voucherSelected = ""
                                }.disabled(hourSelected == 0 || finalHour == 0)
                            }
                            ButtonView(title:"Reiniciar reserva"){
                                hourSelected = 0
                                roomSelected = ""
                                finalHour = 0
                                voucherSelected = ""
                            }
                        }
                       
                    }
                    if (bookingViewModel.messageError != ""){
                        Text(bookingViewModel.messageError).padding()
                    }
                }
            }.onAppear{
                roomViewModel.getAllActiveRooms()
        }
    }
}

struct NewBookingView_Previews: PreviewProvider {
    static var previews: some View {
        NewBookingView()
    }
}
