import SwiftUI

struct BookingFinalHoursView: View {
    
    @Binding var roomSelected:String
    @Binding var currentDate:Date
    @State var hourSelected:Int
    @Binding var finalHour:Int
    @StateObject var bookingViewModel = BookingViewModel()
    
    var body: some View {
        VStack(spacing:16){
            Form{
                if(hourSelected != 0){
                    Text("Selecciona la hora de FINAL").padding().font(.body)
                    Group{
                        ForEach (hourSelected..<23){ i in
                            if(bookingViewModel.takenFinHours.contains(i) == false && i != hourSelected){
                                Button(action:{
                                    finalHour = i
                                },label: {
                                    Text("\(i):00")
                                })
                            }
                        }
                    }.onAppear{
                        bookingViewModel.availableRoomFin(idRoom:roomSelected, date:currentDate, hourSelected:hourSelected)
                    }
                }
            }
        }
    }
}
