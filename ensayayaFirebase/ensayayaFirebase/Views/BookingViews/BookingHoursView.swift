import SwiftUI

struct BookingHoursView: View {
    
    @Binding var roomSelected:String
    @Binding var currentDate:Date
    @Binding var hourSelected:Int
    @StateObject var bookingViewModel = BookingViewModel()
    
    var body: some View {
        VStack(spacing:16){
            Form{
                Text("Selecciona la hora de INICIO").padding().font(.body)
                Group{
                    ForEach (17..<22){ i in
                        if(bookingViewModel.takenIniHours.contains(i) == false){
                            Button(action:{
                                hourSelected = i
                            },label: {
                                Text("\(i):00")
                            })
                        }
                    }
                }.onAppear{
                    //bookingViewModel.addTakenHours(hourSelected:hourSelected)
                    bookingViewModel.availableRoom(idRoom:roomSelected, date:currentDate);
                }
            }
        }
    }
}
