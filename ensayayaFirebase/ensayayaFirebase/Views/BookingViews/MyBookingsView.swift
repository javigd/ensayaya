import SwiftUI

struct MyBookingsView: View {
    
    @StateObject var bookingViewModel = BookingViewModel()
    
    var body: some View {
        VStack(spacing:16){
            Text("MIS RESERVAS").font(.title)
            ForEach(bookingViewModel.bookings, id:\.id){ booking in
                if(booking.FechaReserva >= Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!){
                    Text("Reserva en \(booking.idRoom): \(booking.FechaReserva, style:.date) \(booking.horaInicio):00 - \(booking.horaFin):00")
                }
            }
            Spacer()
        }.padding()
         .onAppear{
             bookingViewModel.bookingsByUser(id:UserDefaults.standard.string(forKey: "id") ?? "")
        }
    }
}

struct MyBookingsView_Previews: PreviewProvider {
    static var previews: some View {
        MyBookingsView()
    }
}
