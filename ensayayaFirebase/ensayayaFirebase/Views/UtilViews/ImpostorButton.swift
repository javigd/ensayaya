import SwiftUI

struct ImpostorButton: View {
    
    let title:String
    private let cornerRadius:CGFloat = 10
    
    var body: some View {
        Text(title)
            .frame(maxWidth:.infinity, maxHeight: 50)
            .background(Color.black)
            .foregroundColor(Color.white)
            .font(.system(size:16, weight:.bold))
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color.clear, lineWidth: 2)
            )
    }
}

struct ImpostorButton_Previews: PreviewProvider {
    static var previews: some View {
        ImpostorButton(title: "Pulsa aquí")
    }
}
