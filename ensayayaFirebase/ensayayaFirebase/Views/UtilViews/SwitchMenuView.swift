import SwiftUI

struct SwitchMenuView: View {
    
    let menu:String
    
    var body: some View {
        switch menu {
            //*********************** Estas son para cliente *************************
        case "MyAccountView":
            MyAccountView()
        case "MyBookingsView":
            MyBookingsView()
        case "BookingView":
            NewBookingView()
        case "ContactView":
            ContactView()
        case "SetListView":
            SetListView()
            //********************** Estas son para admin ***********************
        case "RoomsView":
            RoomsView()
        case "MaterialsView":
            MaterialsView()
        case "VouchersView":
            VouchersView()
        case "UsersView":
            UsersView()
        case "AdminsView":
            AdminsView()
        case "AdminAccountView":
            MyAccountView()
        default:
            SignInView()
        }
    }
}

struct SwitchMenuView_Previews: PreviewProvider {
    static var previews: some View {
        SwitchMenuView(menu: "MyAccountView")
    }
}
