import SwiftUI

struct EditSetListView: View {
    
    @State var selectKeeper = Set<String>()
    @State var idSetList:String
    @StateObject var setListViewModel = SetListViewModel()
    
    var body: some View {
        NavigationView{
            VStack(spacing: 15) {
                List(selection: $selectKeeper) {
                    if let setList = setListViewModel.setListString{
                        ForEach(setList, id:\.self){ cancion in
                                Text(cancion)
                        }
                    }
                }.navigationBarItems(trailing: EditButton())
                if(setListViewModel.messageError != ""){
                    Text(setListViewModel.messageError)
                }
                ButtonView(title:"Eliminar canciones"){
                    setListViewModel.removeSongs(songs:selectKeeper, idSetList:idSetList)
                }.padding()
            
                    .onAppear{setListViewModel.getSetList(idUser: UserDefaults.standard.string(forKey: "id")!, string:true)}
            }
        }
    }
}

struct EditSetListView_Previews: PreviewProvider {
    static var previews: some View {
        EditSetListView(idSetList: "1")
    }
}
