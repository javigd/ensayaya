import SwiftUI
import Combine

struct CreateSetListView: View {
    
    @State var song:String = ""
    @State var minutes:String = ""
    @State var seconds:String = ""
    @State var idSetList:String
    @StateObject var setListViewModel = SetListViewModel()
    
    var body: some View {
        NavigationView{
                VStack(spacing:16){
                    InputTextFieldView(text: $song, placeholder: "Nombre de la canción", keyboardType: .namePhonePad, sfSymbol: "music.quarternote.3").padding().onReceive(song.publisher.collect()) {
                        self.song = String($0.prefix(25))
                    }
                    TextField("Minutos de duración: ", text:$minutes).keyboardType(.decimalPad).padding()
                        .onReceive(minutes.publisher.collect()) {
                                self.minutes = String($0.prefix(2))
                            }
                        .onReceive(Just(minutes)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.minutes = filtered
                            }
                    }
                    TextField("Segundos de duración: ", text:$seconds).keyboardType(.decimalPad).padding()
                        .onReceive(seconds.publisher.collect()) {
                                self.seconds = String($0.prefix(2))
                            }
                        .onReceive(Just(seconds)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.seconds = filtered
                            }
                    }
                    if(setListViewModel.messageError != ""){
                        Text(setListViewModel.messageError)
                    }
                    ButtonView(title:"Añadir Canción"){
                        setListViewModel.addSong(title:song, minutes:minutes, seconds:seconds, idSetList:idSetList)
                    }.disabled(seconds.isEmpty || minutes.isEmpty || song.isEmpty).padding()
                }.applyClose()
        }
    }
}

struct CreateSetListView_Previews: PreviewProvider {
    static var previews: some View {
        CreateSetListView(idSetList: "1")
    }
}
