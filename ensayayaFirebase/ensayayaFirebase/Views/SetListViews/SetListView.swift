import SwiftUI

struct SetListView: View {
    
    @State var showEditSetList:Bool = false
    @State var showCreateSetList:Bool = false
    @State var isShowingConfirmation:Bool = false
    @StateObject var setListViewModel = SetListViewModel()
    
    var body: some View {
        VStack(spacing:16){
            Text("Mi SetList").font(.title).padding()
            if (setListViewModel.messageError != ""){
                Text(setListViewModel.messageError).padding().foregroundColor(.red)
            }
            if let setList = setListViewModel.setList?.canciones{
                ForEach(setList, id:\.self){ cancion in
                    Text("\(cancion.titulo) - \(cancion.duracion)")
                }
            Text("Duración total SetList: \(setListViewModel.setList!.duracionTotal)")
            ButtonView(title:"Editar SetList"){
                showEditSetList.toggle()
            }.padding().sheet(isPresented:$showEditSetList, content:{
                EditSetListView(idSetList:setListViewModel.setList?.id ?? "")
            })
            ButtonView(title:"Eliminar SetList"){
                isShowingConfirmation = true
            }.padding().confirmationDialog("¿Estás seguro de eliminar la SetList?", isPresented:$isShowingConfirmation, titleVisibility: .visible){
                Button("Confirmar", role:.destructive){
                    setListViewModel.deleteSetList(idSetList: setListViewModel.setList?.id ?? "")
                }
                Button("Cancelar", role:.cancel){}
            }
            
        }
            ButtonView(title:"Añadir Canción"){
                showCreateSetList.toggle()
            }.padding().sheet(isPresented:$showCreateSetList, content:{
                CreateSetListView(idSetList:setListViewModel.setList?.id ?? "")
            })
            Spacer()
        }
        .onAppear{
            setListViewModel.getSetList(idUser: UserDefaults.standard.string(forKey: "id")!)
        }
    }
}

struct SetListView_Previews: PreviewProvider {
    static var previews: some View {
        SetListView()
    }
}
