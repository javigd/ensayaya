import Foundation

final class BookingViewModel:ObservableObject{
    
    @Published var bookings:[Booking] = []
    @Published var takenIniHours:[Int] = []
    @Published var takenFinHours:[Int] = []
    @Published var messageError:String = ""
    private let bookingRepository:BookingRepository
    
    init(bookingRepository:BookingRepository = BookingRepository()){
        self.bookingRepository = bookingRepository
    }
    
    func availableRoom(idRoom:String, date:Date){
        bookingRepository.availableRoom(idRoom: idRoom, date:date) { [weak self] result in
            switch result {
                case .success(let bookings):
                for booking in bookings{
                    if( Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)! == booking.FechaReserva){
                        let numHoras = booking.horaFin - booking.horaInicio
                        for i in 0..<numHoras{
                            self?.takenIniHours.append(booking.horaInicio + i)
                        }
                    }
                }
                let hour = Calendar.current.component(.hour, from: Date())
                let firstValue = self?.takenIniHours.sorted().first
                if (firstValue != nil){
                    if (hour > firstValue ?? 0){
                        self?.takenIniHours.append(firstValue! - 1)
                        self?.takenIniHours.append(firstValue! - 2)
                        self?.takenIniHours.append(firstValue! - 3)
                        self?.takenIniHours.append(firstValue! - 4)
                        self?.takenIniHours.append(firstValue! - 5)
                        self?.takenIniHours.append(firstValue! - 6)
                    }
                }
                case .failure(let error):
                    print("Error obteniendo las horas libres \(error)")
            }
        }
    }
    
    func booking(date:Date, idRoom:String, horaInicio:Int, horaFin:Int, bono:String, usosBono:Int){
        self.messageError = ""
        bookingRepository.booking(date:date, idRoom:idRoom, horaInicio:horaInicio, horaFin:horaFin, bono:bono, usosBono:usosBono)
        self.messageError = "Reserva Realizada"
    }
    
    func bookingsByUser(id:String){
        bookingRepository.bookingsByUser(id: id) { [weak self] result in
            switch result {
                case .success(let bookings):
                    self?.bookings = bookings
                case .failure(let error):
                    print("Error obteniendo las horas libres \(error)")
            }
        }
    }
    
    func bookingMaterial(idUser:String, materials:Set<String>){
        bookingRepository.bookingMaterial(idUser:idUser){ [weak self] result in
            switch result {
                case .success(let bookings):
                var dentro:Bool = false
                let materialsArray = Array(materials)
                for booking in bookings{
                    if (booking.FechaReserva == Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!){
                        self?.bookingRepository.bookingMaterialFinal(id:booking.id ?? "", materials:materialsArray)
                        dentro = true;
                    }
                }
                if (dentro == false){
                    self?.messageError = "No existe ninguna reserva a fecha de hoy"
                }
                case .failure(let error):
                    print("Error añadiendo materiales \(error)")
            }
        }
    }
    
    func roomBookings(idRoom:String){
        bookingRepository.roomBookings(idRoom:idRoom){ [weak self] result in
            switch result{
            case.success(let bookings):
                self?.bookings = bookings
            case.failure (let error):
                print("Error obteniendo los datos \(error)")
            }
        }
    }
    
    func availableRoomFin(idRoom:String, date:Date, hourSelected:Int){
        bookingRepository.availableRoom(idRoom: idRoom, date:date) { [weak self] result in
            switch result {
                case .success(let bookings):
                for booking in bookings{
                    if(Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)! == booking.FechaReserva){
                        let numHoras = booking.horaFin - booking.horaInicio
                        for i in 1..<numHoras{
                            self?.takenFinHours.append(booking.horaInicio + i)
                        }
                    }
                }
                let lastValue = self?.takenFinHours.sorted().last
                let firstValue = self?.takenFinHours.sorted().first
                if (lastValue !=  nil && firstValue != nil){
                    if (hourSelected < self?.takenFinHours.sorted().first ?? 0){
                        self?.takenFinHours.append(Int(lastValue! + 1))
                        self?.takenFinHours.append(Int(lastValue! + 2))
                        self?.takenFinHours.append(Int(lastValue! + 3))
                        self?.takenFinHours.append(Int(lastValue! + 4))
                        self?.takenFinHours.append(Int(lastValue! + 5))
                    }
                    else if (hourSelected > self?.takenFinHours.sorted().last ?? 0){
                        self?.takenFinHours.append(firstValue! - 1)
                        self?.takenFinHours.append(firstValue! - 2)
                        self?.takenFinHours.append(firstValue! - 3)
                        self?.takenFinHours.append(firstValue! - 4)
                        self?.takenFinHours.append(firstValue! - 5)
                    }
                }
                case .failure(let error):
                    print("Error obteniendo las horas libres \(error)")
            }
        }
    }
    
    func deleteOldBookings() -> String{
        self.messageError = ""
        guard let deleteDate = Calendar.current.date(byAdding: .month, value: -2, to: Date()) else { return "No se borra" }
        bookingRepository.deleteOldBookings(deleteDate:deleteDate)
        self.messageError = "Reservas antiguas eliminadas"
        return "No se borra"
    }
}

//Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)!
