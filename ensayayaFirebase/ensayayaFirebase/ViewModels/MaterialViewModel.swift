import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

final class MaterialViewModel:ObservableObject{
    
    @Published var materials:[Material] = []
    @Published var materialsName:[String] =  []
    @Published var messageError = ""
    var materialsSaved:[Material] = []
    private let materialRepository:MaterialRepository
    
    init(materialRepository:MaterialRepository = MaterialRepository()){
        self.materialRepository = materialRepository
    }
    
    func getAllActiveMaterials(){
        self.messageError = ""
        materialRepository.getAllActiveMaterials() { [weak self] result in
            switch result {
            case .success(let allMaterial):
                self?.materials = allMaterial
                self?.materialsSaved = allMaterial
            case .failure(let error):
                print("Error obteniendo los materiales \(error)")
            }
        }
    }
    
    func getAllMaterials(){
        self.messageError = ""
        materialRepository.getAllMaterials(){[weak self] result in
            switch result {
            case .success(let allMaterial):
                self?.materials  = allMaterial
                self?.materialsSaved = allMaterial
            case .failure(let error):
                print("Error obteniendo los materials \(error)")
            }
        }
    }
    
    func deleteMaterial(idMaterial:String){
        self.messageError = ""
        materialRepository.deleteMaterial(idMaterial:idMaterial)
    }
    
    func activateMaterial(idMaterial:String){
        self.messageError = ""
        materialRepository.activateMaterial(idMaterial:idMaterial)
        self.messageError = "Material Activado"
    }
    
    func desactivateMaterial(idMaterial:String){
        self.messageError = ""
        materialRepository.desactivateMaterial(idMaterial:idMaterial)
        self.messageError = "Material Desactivado"
    }
    
    func updateMaterial(idMaterial:String, name:String){
        self.messageError = ""
        materialRepository.updateMaterial(idMaterial:idMaterial, name:name)
        self.messageError = "Material Actualizado"
    }
    
    func createMaterial(name:String, active:Bool){
        self.messageError = ""
        materialRepository.createMaterial(name:name, active:active)
        self.messageError = "Material Creado"
    }
    
    func getAllActiveMaterials(string:Bool){
        self.messageError = ""
        materialRepository.getAllActiveMaterials() { [weak self] result in
            switch result {
            case .success(let allMaterial):
                for material in allMaterial{
                    self?.materialsName.append(material.name)
                }
            case .failure(let error):
                print("Error obteniendo los materiales \(error)")
            }
        }
    }
    
   func filterMaterial(filter:String){
       self.messageError = ""
       self.materials = self.materialsSaved
       let materialsAux = self.materials
        self.materials = []
        for material in materialsAux{
            let materialLower = material.name.lowercased()
            let filterLower = filter.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
            if(filterLower == materialLower){
                self.materials.append(material)
            }
        }
       if materials.count <= 0{
           self.messageError = "No hay materiales con ese nombre"
       }
    }
}


//(material.id == mBooking.idMaterial && (horaInicio <= mBooking.horaFin && horaFin >= mBooking.horaInicio))
