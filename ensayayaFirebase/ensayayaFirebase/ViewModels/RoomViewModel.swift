import Foundation


final class RoomViewModel:ObservableObject{
    
    @Published var rooms:[Room] = []
    @Published var messageError:String = ""
    var roomsSaved:[Room] = []
    private let roomRepository:RoomRepository
    
    init(roomRepository:RoomRepository = RoomRepository()){
        self.roomRepository = roomRepository
    }
    
    func getAllRooms(){
        self.messageError = ""
        roomRepository.getAllRooms() { [weak self] result in
            switch result {
            case .success(let allRooms):
                self?.rooms = allRooms
                self?.roomsSaved = allRooms
            case .failure(let error):
                print("Error obteniendo las salas \(error)")
            }
        }
    }
    
    func deleteRoom(idRoom:String) {
        self.messageError = ""
        roomRepository.deleteRoom(idRoom:idRoom){ [weak self] result in
            switch result {
            case .success(let resultado):
                if (resultado == ""){
                    self?.messageError = "Sala Eliminada"
                }else{
                    self?.messageError = "Hay reservas futuras. Operación cancelada"
                }
            case .failure(let error):
                print("Error eliminando las salas \(error)")
            }
        }
    }
    
    func activateRoom(idRoom:String){
        self.messageError = ""
        roomRepository.activateRoom(idRoom:idRoom)
        self.messageError = "Sala Activada"
    }
    
    func desactivateRoom(idRoom:String){
        self.messageError = ""
        roomRepository.desactivateRoom(idRoom:idRoom)
        self.messageError = "Sala Desactivada"
    }
    
    func updateRoom(idRoom:String, title:String, description:String){
        self.messageError = ""
        roomRepository.updateRoom(idRoom:idRoom, title:title, description:description)
        self.messageError = "Sala Actualizada"
    }
    
    func createRoom(title:String, description:String,active:Bool){
        self.messageError = ""
        roomRepository.createRoom(title:title, description:description, active:active)
        self.messageError = "Sala Creada"
    }
    
    func getAllActiveRooms(){
        self.messageError = ""
        roomRepository.getAllActiveRooms() { [weak self] result in
            switch result {
            case .success(let allRooms):
                self?.rooms = allRooms
            case .failure(let error):
                print("Error obteniendo las salas \(error)")
            }
        }
    }
    
    func filterRooms(filter:String){
        self.messageError = ""
        self.rooms = self.roomsSaved
        let roomsAux = self.rooms
        self.rooms = []
        for room in roomsAux{
            let roomLower = room.title.lowercased()
            let filterLower = filter.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
            if(filterLower == roomLower){
                self.rooms.append(room)
            }
        }
        if rooms.count <= 0{
            self.messageError = "No hay materiales con ese nombre"
        }
    }
}
