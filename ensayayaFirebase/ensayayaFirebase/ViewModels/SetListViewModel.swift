import Foundation

final class SetListViewModel:ObservableObject{
    
    @Published var setList:SetList?
    @Published var noSetList:String = ""
    @Published var messageError:String = ""
    @Published var setListString:[String] = []
    
    private let setListRepository:SetListRepository
    
    init(setListRepository:SetListRepository = SetListRepository()){
        self.setListRepository = setListRepository
    }
    
    func getSetList(idUser:String){
        self.messageError = ""
        setListRepository.getSetList(idUser: idUser){ [weak self] result in
            switch result {
                case .success(let setList):
                    self?.setList = setList[0]
                case .failure(let error):
                    print("Error obteniendo la SetList \(error)")
            }
        }
    }
    
    func deleteSetList(idSetList:String){
        self.messageError = ""
        setListRepository.deleteSetList(idSetList: idSetList)
    }
    
    func addSong(title:String, minutes:String, seconds:String, idSetList:String){
        self.messageError = ""
        setListRepository.addSong(title:title, minutes:minutes, seconds:seconds, idSetList: idSetList)
        self.messageError = "¡Canción añadida!"
    }
    
    func removeSongs(songs:Set<String>, idSetList:String){
        self.messageError = ""
        setListRepository.removeSongs(songs:songs, idSetList:idSetList)
        self.messageError = "Se ha eliminado las canciones seleccionadas"
    }
    
    func getSetList(idUser:String, string:Bool){
        self.messageError = ""
        setListRepository.getSetList(idUser: idUser){ [weak self] result in
            switch result {
                case .success(let setList):
                if (setList.count <= 0){
                    self?.messageError = "Su setlist aún no tiene canciones"
                }else{
                    self?.setList = setList[0]
                    for cancion in self?.setList?.canciones ?? []{
                        self?.setListString.append("\(cancion.titulo)-\(cancion.duracion)")
                    }
                }
                case .failure(let error):
                    print("Error obteniendo la SetList \(error)")
            }
        }
    }
}
