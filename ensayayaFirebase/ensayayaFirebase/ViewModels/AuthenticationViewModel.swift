import Foundation

final class AuthenticationViewModel: ObservableObject{
    
    @Published var user:User?
    @Published var messageError:String?
    @Published var errorPassword:String = ""
    @Published var messageLogin:String?
    
    private let authenticationRepository:AuthenticationRepository
    
    init(authenticationRepository:AuthenticationRepository = AuthenticationRepository()){
        self.authenticationRepository = authenticationRepository
        self.getCurrentUser() 
    }
    
    func getCurrentUser(){
        self.user = authenticationRepository.getCurrentUser()
    }
    
    func createNewUser(name:String, surname:String, phone:String, email:String, password:String, username:String){
        self.messageError = ""
        authenticationRepository.createNewUser(name:name, surname:surname,  phone:phone, email:email, password:password, username:username){ [weak self] result in
            switch result {
            case .success(let user):
                self?.user = user
            case .failure(let error):
                self?.messageError = "Email no disponible o inválido"
            }
        }
    }
    
    func login(email:String, password:String){
        self.messageLogin = ""
        authenticationRepository.login(email: email, password: password){ [weak self] result in
            switch result {
            case .success(let user):
                self?.user = user
                print(user.verified)
            case .failure(let error):
                self?.messageLogin = "Email o contraseña incorrectos"
            }
        }
    }
    
    func logout() {
        self.messageError = ""
        do {
            try authenticationRepository.logout()
            self.user = nil
            print("hacelogout")
        }catch{
            print("Error logout")
        }
    
    }
    
    func rememberPassword(email:String){
        self.messageError = ""
        do{
            try authenticationRepository.rememberPassword(email: email)
            print("Correo enviado sin problemas")
        }catch{
            print("Error al recordar la contraseña")
            self.messageError = "Error al recordar la contraseña"
        }
    }
    
    func changePassword(password:String){
        self.errorPassword = ""
        do{
            try authenticationRepository.changePassword(password:password)
        }catch{
            print("Error al cambiar la contraseña")
            self.errorPassword = "Error al cambiar la contraseña"
        }
    }
    
    func changeEmail(email:String){
        self.messageError = ""
        do{
            try authenticationRepository.changeEmail(email:email)
        }catch{
            print("Error al cambiar el email")
            self.messageError = "Error al cambiar el email"
        }
    }
    
    func createNewAdmin(name:String, surname:String, phone:String, email:String, password:String, username:String){
        self.messageError = ""
        authenticationRepository.createNewAdmin(name:name, surname:surname,  phone:phone, email:email, password:password, username:username){ [weak self] result in
            switch result {
            case .success(let user):
                self?.messageError = "Cuenta registrada"
            case .failure(let error):
                self?.messageError = "Ya existe una cuenta con ese email"
            }
        }
    }
    
    func deleteAccount(idUser:String){
        self.messageError = ""
        authenticationRepository.deleteAccount(idUser:idUser)
        self.user = nil
    }
}
