import Foundation

final class UserDataViewModel: ObservableObject{
    
    @Published var users:[UserData] = []
    @Published var messageError:String = ""
    var usersSaved:[UserData] = []
    private let userDataRepository:UserDataRepository
    
    init(userDataRepository:UserDataRepository = UserDataRepository()){
        self.userDataRepository = userDataRepository
    }
    
    func setNewUserData(name:String, surname:String, phone:String){
        self.messageError = ""
        do{
            try userDataRepository.setNewUserData(name:name, surname:surname, phone:phone)
        }catch{
            self.messageError = "Error actualizando datos de usuario"
            print("Error actualizando datos de usuario")
        }
    }
    
    func getAllClients(){
        self.messageError = ""
        userDataRepository.getAllClients() { [weak self] result in
            switch result {
            case .success(let allUsers):
                self?.users = allUsers
                self?.usersSaved = allUsers
            case .failure(let error):
                print("Error obteniendo los usuarios \(error)")
            }
        }
    }
    
    func disableAccount(id:String){
        self.messageError = ""
        if (id == "jorGx5dzYlcsio78OEdslUrB7lx2"){
            self.messageError = "Cuenta de administrador principal, no es posible desactivarla"
        }else{
            userDataRepository.disableAccount(id: id)
            self.messageError = "Cuenta desactivada"
        }
    }
    
    func enableAccount(id:String){
        self.messageError = ""
        userDataRepository.enableAccount(id: id)
        self.messageError = "Cuenta activada"
    }
    
    func filterAllClients(filter:String){
        self.messageError = ""
        self.users = self.usersSaved
        let filterLower = filter.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        let usersAux = self.users
        self.users = []
        for user in usersAux{
            let fullName = user.name.lowercased() + " " + user.surname.lowercased()
            if(filterLower == fullName){
                self.users.append(user)
            }
        }
        if users.count <= 0{
            self.messageError = "No hay usuarios con ese nombre - apellidos"
        }
    }
    
    func getAllAdmins(){
        self.messageError = ""
        userDataRepository.getAllAdmins(){ [weak self] result in
            switch result {
            case .success(let allAdmins):
                self?.users = allAdmins
                self?.usersSaved = allAdmins
            case .failure(let error):
                print("Error obteniendo los admins \(error)")
            }
        }
    }
}
