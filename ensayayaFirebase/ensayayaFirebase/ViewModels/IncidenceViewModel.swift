import Foundation

final class IncidenceViewModel:ObservableObject{
    
    @Published var incidences:[Incidence] = []
    @Published var messageError:String = ""
    private let incidenceRepository:IncidenceRepository
    
    init(incidenceRepository:IncidenceRepository = IncidenceRepository()){
        self.incidenceRepository = incidenceRepository
    }
    
    func getIncidenceById(id:String){
        self.messageError = ""
        incidenceRepository.getIncidenceById(id: id) { [weak self] result in
            switch result {
                case .success(let incidencesById):
                    self?.incidences = incidencesById
                case .failure(let error):
                    print("Error obteniendo las incidencias \(error)")
            }
        }
    }
    
    func createIncidence(id:String, description:String){
        self.messageError = ""
        incidenceRepository.createIncidence(id:id, description:description)
        self.messageError = "Incidencia Creada"
    }
    
    func resolveIncidence(idIncidence:String){
        self.messageError = ""
        incidenceRepository.resolveIncidence(idIncidence:idIncidence)
        self.messageError = "Incidencia resuelta"
    }
}
