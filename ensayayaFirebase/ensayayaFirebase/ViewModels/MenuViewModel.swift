import Foundation

final class MenuViewModel: ObservableObject{
    
    @Published var menu:[Menu] = []
    
    private let menuRepository:MenuRepository
    
    init(menuRepository:MenuRepository = MenuRepository()){
        self.menuRepository = menuRepository
    }
    
    func getMenuOptions() {
        menuRepository.getMenuOptions() { [weak self] result in
            switch result {
            case .success(let optionsMenu):
                self?.menu = optionsMenu
            case .failure(let error):
                print("Error en el MenuViewModel obteniendo las opciones del menú \(error)")
            }
        }
    }
    
}
