import Foundation

final class VoucherViewModel:ObservableObject{
    
    @Published var vouchers:[Voucher] = []
    @Published var uVoucher:[Uvoucher] = []
    @Published var messageError:String = ""
    
    private let voucherRepository:VoucherRepository
    
    init(voucherRepository:VoucherRepository = VoucherRepository()){
        self.voucherRepository = voucherRepository
    }
    
    func getAllVouchers(){
        self.messageError = ""
        voucherRepository.getAllVouchers() { [weak self] result in
            switch result {
            case .success(let allVouchers):
                self?.vouchers = allVouchers
            case .failure(let error):
                print("Error obteniendo los materiales \(error)")
            }
        }
    }
    
    func updateVoucher(idVoucher:String, usages:String ,name:String, price:String){
        self.messageError = ""
        voucherRepository.updateVoucher(idVoucher:idVoucher, name:name, usages:Int(usages)!, price:Double(price)!)
        self.messageError = "Bono Actualizado"
    }
    
    func deleteVoucher(idVoucher:String){
        self.messageError = ""
        voucherRepository.deleteVoucher(idVoucher:idVoucher)
        self.messageError = "Bono Eliminado"
    }
    
    func createVoucher(usages:String, price:String, name:String){
        self.messageError = ""
        voucherRepository.createVoucher(usages:Int(usages)!, name:name, price:Double(price)!)
        self.messageError = "Bono Creado"
    }
    
    func buyVoucher(idUser:String, name:String, usages:Int){
        self.messageError = ""
        voucherRepository.buyVoucher(idUser: idUser, usages: usages, name: name)
        self.messageError = "Bono Comprado"
    }
    
    func voucherPerId(idUser:String){
        self.messageError = ""
        voucherRepository.voucherPerId(idUser:idUser) { [weak self] result in
            switch result {
            case .success(let voucher):
                self?.uVoucher = voucher
            case .failure(let error):
                print("Error obteniendo los materiales \(error)")
            }
        }
    }
}
