import Foundation

final class AuthenticationRepository {
    private let authenticationFirebaseDatasource: AuthenticationFirebaseDatasource
    
    init(authenticationFirebaseDatasource: AuthenticationFirebaseDatasource = AuthenticationFirebaseDatasource()){
        self.authenticationFirebaseDatasource = authenticationFirebaseDatasource
    }
    
    func getCurrentUser() -> User?{
        authenticationFirebaseDatasource.getCurrentUser()
    }
    
    func createNewUser(name:String, surname:String, phone:String, email:String, password:String, username:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        authenticationFirebaseDatasource.createNewUser(name:name, surname:surname,  phone:phone,email: email, password: password, username:username, completionBlock: completionBlock)
    }
    
    func login(email:String, password:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        authenticationFirebaseDatasource.login(email: email, password: password, completionBlock: completionBlock)
    }
    
    func logout() throws{
        try authenticationFirebaseDatasource.logout()
    }
    
    func rememberPassword(email:String) throws{
        try authenticationFirebaseDatasource.rememberPassword(email: email)
    }
    
    func changePassword(password:String) throws{
        try authenticationFirebaseDatasource.changePassword(password: password)
    }
    func changeEmail(email:String) throws{
        try authenticationFirebaseDatasource.changeEmail(email:email)
    }
    
    func createNewAdmin(name:String, surname:String, phone:String, email:String, password:String, username:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        authenticationFirebaseDatasource.createNewAdmin(name:name, surname:surname,  phone:phone,email: email, password: password, username:username, completionBlock: completionBlock)
    }
    
    func deleteAccount(idUser:String){
      try  authenticationFirebaseDatasource.deleteAccount(idUser:idUser)
    }
}
