import Foundation


final class RoomRepository{
    
    private let roomDataSource:RoomDataSource
    
    init(roomDataSource: RoomDataSource = RoomDataSource()){
        self.roomDataSource = roomDataSource
    }
    
    func getAllRooms(completionBlock: @escaping (Result<[Room], Error>) -> Void) {
        roomDataSource.getAllRooms(completionBlock: completionBlock)
    }
    
    func deleteRoom(idRoom:String, completionBlock: @escaping (Result<String, Error>) -> Void){
        roomDataSource.deleteRoom(idRoom:idRoom, completionBlock: completionBlock)
    }
    
    func activateRoom(idRoom:String){
        roomDataSource.activateRoom(idRoom:idRoom)
    }
    
    func desactivateRoom(idRoom:String){
        roomDataSource.desactivateRoom(idRoom:idRoom)
    }
    
    func updateRoom(idRoom:String, title:String, description:String){
        roomDataSource.updateRoom(idRoom:idRoom, title:title, description:description)
    }
    
    func createRoom(title:String, description:String, active:Bool){
        roomDataSource.createRoom(title:title, description:description, active:active)
    }
    
    func getAllActiveRooms(completionBlock: @escaping (Result<[Room], Error>) -> Void) {
        roomDataSource.getAllActiveRooms(completionBlock: completionBlock)
    }
}
