import Foundation

final class UserDataRepository{
    private let userDataDataSource:UserDataDataSource
    
    init(userDataDataSource: UserDataDataSource = UserDataDataSource()){
        self.userDataDataSource = userDataDataSource
    }
    
    
    func setNewUserData(name:String, surname:String, phone:String) throws{
        try userDataDataSource.setNewUserData(name:name, surname:surname, phone:phone)
    }
    
    func getAllClients(completionBlock: @escaping (Result<[UserData], Error>) -> Void){
        userDataDataSource.getAllClients(completionBlock:  completionBlock)
    }
    
    func disableAccount(id:String){
        userDataDataSource.disableAccount(id: id)
    }
    
    func enableAccount(id:String){
        userDataDataSource.enableAccount(id: id)
    }
    
    func getAllAdmins(completionBlock: @escaping (Result<[UserData], Error>) -> Void){
        userDataDataSource.getAllAdmins(completionBlock:  completionBlock)
    }
    
}
