import Foundation

final class IncidenceRepository{
    
    private let incidenceDataSource:IncidenceDataSource
    
    init(incidenceDataSource: IncidenceDataSource = IncidenceDataSource()){
        self.incidenceDataSource = incidenceDataSource
    }
    
    func getIncidenceById(id:String, completionBlock: @escaping (Result<[Incidence], Error>) -> Void) {
        incidenceDataSource.getIncidenceById(id: id, completionBlock: completionBlock)
    }
    
    func createIncidence(id:String, description:String){
        incidenceDataSource.createIncidence(id: id, description: description)
    }
    
    func resolveIncidence(idIncidence:String){
        incidenceDataSource.resolveIncidence(idIncidence:idIncidence)
    }
}
