import Foundation

final class MenuRepository{
    private let menuDataSource:MenuDataSource
    
    init(menuDataSource: MenuDataSource = MenuDataSource()){
        self.menuDataSource = menuDataSource
    }
    
    
    func getMenuOptions(completionBlock: @escaping (Result<[Menu], Error>) -> Void) {
        menuDataSource.getMenuOptions(completionBlock:  completionBlock)
    }
}
