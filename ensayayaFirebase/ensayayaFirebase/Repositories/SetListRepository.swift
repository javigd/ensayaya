import Foundation

final class SetListRepository{
    
    private let setListDataSource:SetListDataSource
    
    init(setListDataSource: SetListDataSource = SetListDataSource()){
        self.setListDataSource = setListDataSource
    }
    
    func getSetList(idUser:String,  completionBlock: @escaping (Result<[SetList], Error>) -> Void){
        setListDataSource.getSetList(idUser: idUser, completionBlock: completionBlock)
    }
    
    func deleteSetList(idSetList:String){
        setListDataSource.deleteSetList(idSetList: idSetList)
    }
    
    func addSong(title:String, minutes:String, seconds:String, idSetList:String){
        setListDataSource.addSong(title:title, minutes:minutes, seconds:seconds, idSetList:idSetList)
    }
    
    func removeSongs(songs:Set<String>, idSetList:String){
        setListDataSource.removeSongs(songs:songs, idSetList:idSetList)
    }
}
