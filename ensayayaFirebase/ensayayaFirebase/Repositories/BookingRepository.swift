import Foundation

final class BookingRepository{
    
    private let bookingDataSource:BookingDataSource
    
    init(bookingDataSource: BookingDataSource = BookingDataSource()){
        self.bookingDataSource = bookingDataSource
    }

    func availableRoom(idRoom:String, date:Date, completionBlock: @escaping (Result<[Booking], Error>) -> Void) {
        bookingDataSource.availableRoom(idRoom: idRoom, date:date,completionBlock: completionBlock)
    }
    
    func booking(date:Date, idRoom:String, horaInicio:Int, horaFin:Int, bono:String, usosBono:Int){
        bookingDataSource.booking(date:date, idRoom:idRoom, horaInicio:horaInicio, horaFin:horaFin, bono:bono, usosBono:usosBono)
    }
    
    func bookingsByUser(id:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void) {
        bookingDataSource.bookingsByUser(id: id, completionBlock: completionBlock)
    }
    
    func bookingMaterial(idUser:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        bookingDataSource.bookingMaterial(idUser:idUser, completionBlock: completionBlock)
    }
    
    func bookingMaterialFinal(id:String, materials:[String]){
        bookingDataSource.bookingMaterialFinal(id:id, materials:materials)
    }
    
    func roomBookings(idRoom:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        bookingDataSource.roomBookings(idRoom:idRoom, completionBlock:completionBlock)
    }
    
    func deleteOldBookings(deleteDate:Date){
        bookingDataSource.deleteOldBookings(deleteDate:deleteDate)
    }
}
