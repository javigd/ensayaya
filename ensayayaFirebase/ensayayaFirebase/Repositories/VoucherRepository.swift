import Foundation

final class VoucherRepository{
    
    private let voucherDataSource:VoucherDataSource
    
    init(voucherDataSource: VoucherDataSource = VoucherDataSource()){
        self.voucherDataSource = voucherDataSource
    }
    
    func getAllVouchers(completionBlock: @escaping (Result<[Voucher], Error>) -> Void) {
        voucherDataSource.getAllVouchers(completionBlock: completionBlock)
    }
    
    func updateVoucher(idVoucher:String, name:String, usages:Int, price:Double){
        voucherDataSource.updateVoucher(idVoucher:idVoucher, name:name, usages:usages, price:price)
    }
    
    func deleteVoucher(idVoucher:String){
        voucherDataSource.deleteVoucher(idVoucher:idVoucher)
    }
    
    func createVoucher(usages:Int, name:String, price:Double){
        voucherDataSource.createVoucher(usages:usages, name:name, price:price)
    }
    
    func buyVoucher(idUser:String,usages:Int, name:String){
        voucherDataSource.buyVoucher(idUser:idUser, usages:usages, name:name)
    }
    
    func voucherPerId(idUser:String, completionBlock: @escaping (Result<[Uvoucher], Error>) -> Void) {
        voucherDataSource.voucherPerId(idUser:idUser, completionBlock: completionBlock)
    }
}
