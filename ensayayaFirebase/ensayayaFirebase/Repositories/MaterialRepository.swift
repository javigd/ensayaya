import Foundation

final class MaterialRepository{
    
    private let materialDataSource:MaterialDataSource
    
    init(materialDataSource: MaterialDataSource = MaterialDataSource()){
        self.materialDataSource = materialDataSource
    }
    
    func getAllActiveMaterials(completionBlock: @escaping (Result<[Material], Error>) -> Void) {
        materialDataSource.getAllActiveMaterials(completionBlock: completionBlock)
    }
    
    func getAllMaterials(completionBlock: @escaping (Result<[Material], Error>) -> Void) {
        materialDataSource.getAllMaterials(completionBlock: completionBlock)
    }
    
    func deleteMaterial(idMaterial:String){
        materialDataSource.deleteMaterial(idMaterial:idMaterial)
    }
    
    func activateMaterial(idMaterial:String){
        materialDataSource.activateMaterial(idMaterial:idMaterial)
    }
    
    func desactivateMaterial(idMaterial:String){
        materialDataSource.desactivateMaterial(idMaterial:idMaterial)
    }
    
    func updateMaterial(idMaterial:String, name:String){
        materialDataSource.updateMaterial(idMaterial:idMaterial, name:name)
    }
    
    func createMaterial(name:String, active:Bool){
        materialDataSource.createMaterial(name:name, active:active)
    }
}
