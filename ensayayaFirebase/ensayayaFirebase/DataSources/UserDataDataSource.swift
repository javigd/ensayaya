import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseAuth

struct UserData: Decodable, Identifiable, Hashable{
    @DocumentID var id:String?
    let activo:Bool
    let email:String
    let name:String
    let phone:String
    let role:String
    let surname:String
    let username:String
}

final class UserDataDataSource{
    
    
    var db = Firestore.firestore()
    
    func setNewUserData(name:String, surname:String, phone:String){
        
        let docData:[String:Any] = [
            "name": name,
            "surname":surname,
            "phone":phone,
        ]
        db.collection("users").document(UserDefaults.standard.string(forKey:"id") ?? "").setData(docData, merge:true){ err in
            if let err = err{
                print("Error actualizando registros de usuario: \(err)")
            }else{
                print("Datos de usuario actualizados con éxito")
            }
        }
        //Actualizo los datos de UserDefaults que he modificado para que se actualicen las vistas pertinentes
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.set(surname, forKey: "surname")
        UserDefaults.standard.set(phone, forKey: "phone")
        
    }
    
    func getAllClients(completionBlock: @escaping (Result<[UserData], Error>) -> Void){
        db.collection("users").whereField("role", isEqualTo: "cliente").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let allClients = documents.map { try? $0.data(as: UserData.self)}.compactMap{ $0 }
            completionBlock(.success(allClients))
        }
    }
    
    func disableAccount(id:String){
        db.collection("users").document(id).setData(["activo":false], merge:true)
    }
    
    func enableAccount(id:String){
        db.collection("users").document(id).setData(["activo":true], merge:true)
    }
    
    func getAllAdmins(completionBlock: @escaping (Result<[UserData], Error>) -> Void){
        db.collection("users").whereField("role", isEqualTo: "admin").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let allAdmins = documents.map { try? $0.data(as: UserData.self)}.compactMap{ $0 }
            completionBlock(.success(allAdmins))
        }
    }
}
