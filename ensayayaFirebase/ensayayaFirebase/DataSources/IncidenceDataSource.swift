import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Incidence: Decodable, Identifiable{
    @DocumentID var id:String?
    let description:String
    let notificationDate:Date
    let solutionDate:Date?
    let idObject:String
}

final class IncidenceDataSource{
    
    var db = Firestore.firestore()
    
    func getIncidenceById(id:String, completionBlock: @escaping (Result<[Incidence], Error>) -> Void){
        db.collection("incidences").whereField("idObject", isEqualTo: id).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let incidences = documents.map { try? $0.data(as: Incidence.self)}.compactMap{ $0 }
            completionBlock(.success(incidences))
        }
    }
    
    func createIncidence(id:String, description:String){
        db.collection("incidences").document().setData(["description": description, "idObject": id, "notificationDate":Date()])
    }
    
    func resolveIncidence(idIncidence:String){
        db.collection("incidences").document(idIncidence).setData(["solutionDate":Date()], merge:true)
    }
}
