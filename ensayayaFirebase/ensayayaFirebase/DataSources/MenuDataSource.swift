import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseAuth

struct Menu: Decodable, Identifiable{
    @DocumentID var id:String?
    let label:String
    let role:String
    let title:String
    let systemimage:String
}

final class MenuDataSource{
    
    func getMenuOptions(completionBlock: @escaping (Result<[Menu], Error>) -> Void){
        let db = Firestore.firestore()
        db.collection("menu").whereField("role", isEqualTo: UserDefaults.standard.string(forKey: "role") ?? "").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let menuOptions = documents.map { try? $0.data(as: Menu.self)}.compactMap{ $0 }
            completionBlock(.success(menuOptions))
        }
    }
}
