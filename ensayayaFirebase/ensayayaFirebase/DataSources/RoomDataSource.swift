import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Room: Decodable, Identifiable{
    @DocumentID var id:String?
    let title:String
    let description:String
    let active:Bool
}

final class RoomDataSource{
    
    var db = Firestore.firestore()
    
    func getAllRooms(completionBlock: @escaping (Result<[Room], Error>) -> Void){
        db.collection("rooms").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let rooms = documents.map { try? $0.data(as: Room.self)}.compactMap{ $0 }
            completionBlock(.success(rooms))
        }
    }
    
    func deleteRoom(idRoom:String, completionBlock: @escaping (Result<String, Error>) -> Void){
        var resultado = ""
        db.collection("rooms").document(idRoom).collection("bRoom").whereField("fechaReserva", isGreaterThanOrEqualTo: Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
            }else{
                if query?.isEmpty == false{
                    resultado = "La sala que quiere eliminar no puede ser eliminada ya que tiene reservas futuras"
                    completionBlock(.success(resultado))
                }else{
                    self.db.collection("rooms").document(idRoom).delete()
                }
            }
        }
    }
    
    func activateRoom(idRoom:String){
      db.collection("rooms").document(idRoom).setData(["active":true], merge:true) //El merge lo usamos para combinar datos en firebase, y así no tener que actualizar absolutamente todo
    }
    
    func desactivateRoom(idRoom:String){
        db.collection("rooms").document(idRoom).setData(["active":false], merge:true)
    }
    
    func updateRoom(idRoom:String, title:String, description:String){
        db.collection("rooms").document(idRoom).setData(["title":title, "description":description], merge:true) 
    }
    
    func createRoom(title:String, description:String, active:Bool){
        db.collection("rooms").document(title).setData(["title":title, "description":description, "active": active]) { err in
            if let err = err {
                print("Error creando la sala \(err)")
            }else{
                print("La sala ha sido creada con éxito")
            }
        }
    }

    func getAllActiveRooms(completionBlock: @escaping (Result<[Room], Error>) -> Void){
        db.collection("rooms").whereField("active", isEqualTo:true).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let rooms = documents.map { try? $0.data(as: Room.self)}.compactMap{ $0 }
            completionBlock(.success(rooms))
        }
    }
}
