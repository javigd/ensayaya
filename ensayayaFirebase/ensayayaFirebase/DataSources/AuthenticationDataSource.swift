import Foundation
import FirebaseAuth
import FirebaseFirestoreSwift
import FirebaseFirestore

struct User{
    let email:String
    let verified:Bool
    //Probar a meter nuevos datos aquí
    /*let uid:String
    let name:String
    let surname:String
    let phone:String
    let role:String
    */
}

enum RegistrationKeys:String {
    case name
    case surname
    case phone
    case role
    case username
}



final class AuthenticationFirebaseDatasource{
    
    func getCurrentUser() -> User? {
        guard let email = Auth.auth().currentUser?.email
        else{
            return nil
        }
        guard let verified = Auth.auth().currentUser?.isEmailVerified
        else{
            return nil
        }
        /*guard let uid = Auth.auth().currentUser?.uid
        else{
            return nil
        }
        let name = docData["name"] as? String ?? ""
        let surname = docData["surname"] as? String ?? ""
        let phone = docData["phone"] as? String ?? ""
        let role = docData["role"] as? String ?? "" */
        return .init(email:email, verified:verified)
    }
    
    func createNewUser(name:String, surname:String, phone:String, email:String, password:String, username:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        Auth.auth().createUser(withEmail: email, password: password) { authDataResult, error in
            if let error = error{
                print("Error creating a new user \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            let email = authDataResult?.user.email ?? "No Email"
            let verified = authDataResult?.user.isEmailVerified
            print("New user created with info \(email)")
            completionBlock(.success(.init(email:email, verified: verified!)))
            Auth.auth().currentUser?.sendEmailVerification(){ error in
                if let error = error{
                    print("Error enviando el link de verificación de correo \(error)")
                }
            }
            if let uid = authDataResult?.user.uid{
                let userValues = [
                    RegistrationKeys.name.rawValue: name,
                    RegistrationKeys.surname.rawValue: surname,
                    RegistrationKeys.phone.rawValue: phone,
                    RegistrationKeys.role.rawValue: "cliente",
                    RegistrationKeys.username.rawValue:username,
                    "email":email,
                    "activo":true
                ] as [String:Any]

                let db = Firestore.firestore()
                do{
                    db.collection("users").document(uid).setData(userValues)
                    
                    db.collection("setLists").document().setData(["idUser":uid])
                    print("Datos de usuario introducidos con éxito")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0){
                        do{
                            try self.logout()
                        }catch{
                            print("Fallo al cerrar sesión")
                        }
                    }
                }catch{
                    print(error)
                }
            }else{
                print("Error obteniendo id de usuario")
            }
        }
    }
    
    func logout() throws{
        try Auth.auth().signOut()
        //Al cerrar sesión debo de eliminar todas las variables de sesión que se hayan establecido. Más información acerca de estas en el método de login
        UserDefaults.standard.removeObject(forKey: "id")
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "surname")
        UserDefaults.standard.removeObject(forKey: "phone")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "role")
        UserDefaults.standard.removeObject(forKey: "active")
    }
    
    func login(email:String, password:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        Auth.auth().signIn(withEmail: email, password: password) { authDataResult, error in
            if let error = error{
                print("Error login \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
           //do{
            let email = authDataResult?.user.email ?? "No Email"
            let verified = authDataResult?.user.isEmailVerified
            let db = Firestore.firestore()
            db.collection("users").document((authDataResult?.user.uid)!).getDocument(){ (document, error) in
                guard let document = document, document.exists else {
                    print("Document does not exist")
                    return
                }
                let dataDescription = document.data()
                //Establezo en el objeto que nos proporciona SWIFT (UserDefaults) las variables
                //que necesito, para poder así usarlas por toda la aplicación
                //Para ver cómo se usa esto --> https:// stackoverflow.com/questions/31203241/how-can-i-use-userdefaults-in-swift (quitar el espacio)
                UserDefaults.standard.set(Auth.auth().currentUser!.uid, forKey:"id")
                UserDefaults.standard.set(dataDescription?["name"] as! String, forKey:"name")
                UserDefaults.standard.set(dataDescription?["surname"] as! String, forKey:"surname")
                UserDefaults.standard.set(dataDescription?["phone"] as! String, forKey:"phone")
                UserDefaults.standard.set(dataDescription?["role"] as! String, forKey:"role")
                UserDefaults.standard.set(email, forKey:"email")
                UserDefaults.standard.set(dataDescription?["username"] as! String, forKey: "username")
                UserDefaults.standard.set(dataDescription?["activo"] as! Bool, forKey:"active")
                print("Iniciando sesión rol: \(UserDefaults.standard.string(forKey:"role") ?? "")")
            }
            print("Usuario con cuenta activa --> \(UserDefaults.standard.bool(forKey: "active"))")
            print("Usuario logueado con email: \(email)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                completionBlock(.success(.init(email:email, verified:verified!)))
            }
          /* }catch{
                print("Error durante el login")
            }*/
            
        }
    }
    
    func rememberPassword(email:String){
        Auth.auth().sendPasswordReset(withEmail: email){ error in
            if let error = error {
                print("Error \(error)")
                return
            }
        }
    }
    
    func changePassword(password:String){
        Auth.auth().currentUser?.updatePassword(to: password){ (error) in
            if let error = error{
                print("Error \(error)")
                return
            }
        }
        Auth.auth().sendPasswordReset(withEmail: UserDefaults.standard.string(forKey: "email") ?? "") { error in
            if let error = error{
                print("Error \(error)")
                return
            }
        }
    }

    func changeEmail(email:String){
        Auth.auth().currentUser?.updateEmail(to: email){(error) in
            if let error = error{
                print("Error \(error)")
                return
            }
        }
        //Actualizo la variable global de UserDefaults para tener el nuevo correo en las vistas
        UserDefaults.standard.set(email, forKey: email)
    }
    
    func createNewAdmin(name:String, surname:String, phone:String, email:String, password:String, username:String, completionBlock: @escaping (Result<User, Error>) -> Void){
        Auth.auth().createUser(withEmail: email, password: password) { authDataResult, error in
            if let error = error{
                print("Error creating a new admin \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            let email = authDataResult?.user.email ?? "No Email"
            let verified = authDataResult?.user.isEmailVerified
            print("New user created with info \(email)")
            completionBlock(.success(.init(email:email, verified: verified!)))
            Auth.auth().currentUser?.sendEmailVerification(){ error in
                if let error = error{
                    print("Error enviando el link de verificación de correo \(error)")
                }
            }
            if let uid = authDataResult?.user.uid{
                let userValues = [
                    RegistrationKeys.name.rawValue: name,
                    RegistrationKeys.surname.rawValue: surname,
                    RegistrationKeys.phone.rawValue: phone,
                    RegistrationKeys.role.rawValue: "admin",
                    RegistrationKeys.username.rawValue:username,
                    "email":email,
                    "activo":true
                ] as [String:Any]

                let db = Firestore.firestore()
                do{
                    db.collection("users").document(uid).setData(userValues)
                    print("Datos de admin introducidos con éxito")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                        do{
                            try self.logout()
                        }catch{
                            print("Fallo al cerrar sesión")
                        }
                    }
                }catch{
                    print(error)
                }
            }else{
                print("Error obteniendo id de usuario")
            }
        }
    }
    
    func deleteAccount(idUser:String){
        Firestore.firestore().collection("users").document(idUser).delete()
        Firestore.firestore().collection("setLists").whereField("idUser", isEqualTo: idUser).getDocuments(){ (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    Firestore.firestore().collection("setLists").document(document.documentID).delete()
                }
            }
        }
        Auth.auth().currentUser!.delete{ error in
            if let error = error{
                print("Error eliminándose la cuenta")
            }else{
                UserDefaults.standard.removeObject(forKey: "id")
                UserDefaults.standard.removeObject(forKey: "name")
                UserDefaults.standard.removeObject(forKey: "surname")
                UserDefaults.standard.removeObject(forKey: "phone")
                UserDefaults.standard.removeObject(forKey: "email")
                UserDefaults.standard.removeObject(forKey: "role")
                UserDefaults.standard.removeObject(forKey: "active")
            }
        }
    }
}
