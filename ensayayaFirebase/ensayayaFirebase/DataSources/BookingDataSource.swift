import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Booking: Decodable, Identifiable{
    @DocumentID var id:String?
    var FechaReserva:Date
    var idRoom:String
    var horaInicio:Int
    var horaFin:Int
    var idUser:String
    var idBono:String?
    var materials:[String]?
    var fullName:String
}

final class BookingDataSource{
    
    var db = Firestore.firestore()
    
    func availableRoom(idRoom:String, date:Date, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        
        db.collection("bookings").whereField("idRoom", isEqualTo: idRoom).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let bookings = documents.map { try? $0.data(as: Booking.self)}.compactMap{ $0 }
            completionBlock(.success(bookings))
        }
    }
    
    func booking(date:Date, idRoom:String, horaInicio:Int, horaFin:Int, bono:String, usosBono:Int){
        
        let fullName = UserDefaults.standard.string(forKey: "name")! + " " + UserDefaults.standard.string(forKey: "surname")!
        db.collection("bookings").document().setData(["FechaReserva": Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)!, "idRoom": idRoom, "horaInicio": horaInicio, "horaFin": horaFin, "idUser": UserDefaults.standard.string(forKey: "id") ?? "", "fullName":fullName, "idBono":bono])
        if (bono != ""){
            let difHoras = horaFin - horaInicio
            let usosRest = usosBono - difHoras
            
            if (usosRest <= 0){
                db.collection("users").document(UserDefaults.standard.string(forKey: "id")!).collection("uVoucher").document(bono).delete()
            }else{
                db.collection("users").document(UserDefaults.standard.string(forKey: "id")!).collection("uVoucher").document(bono).setData(["numUsages":usosRest], merge:true)
            }
        }
        db.collection("rooms").document(idRoom).collection("bRoom").document().setData(["fechaReserva":Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)!])
    }
    
    func bookingsByUser(id:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        
        db.collection("bookings").whereField("idUser", isEqualTo: id).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let bookings = documents.map { try? $0.data(as: Booking.self)}.compactMap{ $0 }
            completionBlock(.success(bookings))
        }
    }
    
    func bookingMaterial(idUser:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        
        
       db.collection("bookings").whereField("idUser", isEqualTo: idUser).addSnapshotListener {query, error in
           if let error = error{
               print("Error obteniendo los datos \(error.localizedDescription)")
               completionBlock(.failure(error))
               return
           }
           guard let documents = query?.documents.compactMap({ $0 }) else{
               completionBlock(.success([]))
               return
           }
           let bookings = documents.map { try? $0.data(as: Booking.self)}.compactMap{ $0 }
           completionBlock(.success(bookings))
       }
    }
    
    func bookingMaterialFinal(id:String, materials:[String]){
        db.collection("bookings").document(id).setData(["materials":materials], merge:true)
    }
    
    func roomBookings(idRoom:String, completionBlock: @escaping (Result<[Booking], Error>) -> Void){
        db.collection("bookings").whereField("idRoom", isEqualTo: idRoom).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let bookings = documents.map { try? $0.data(as: Booking.self)}.compactMap{ $0 }
            completionBlock(.success(bookings))
        }
    }
    
    func deleteOldBookings(deleteDate:Date){
        db.collection("bookings").whereField("FechaReserva", isLessThanOrEqualTo: deleteDate).getDocuments(){ (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    self.db.collection("bookings").document(document.documentID).delete()
                }
            }
        }
    }
}
