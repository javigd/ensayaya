import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Material: Decodable, Identifiable, Hashable{
    @DocumentID var id:String?
    let name:String
    let active:Bool
}

final class MaterialDataSource{
    
    var db = Firestore.firestore()
    
    func getAllActiveMaterials(completionBlock: @escaping (Result<[Material], Error>) -> Void){
        db.collection("materials").whereField("active", isEqualTo: true).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let materials = documents.map { try? $0.data(as: Material.self)}.compactMap{ $0 }
            completionBlock(.success(materials))
        }
    }
    
    func getAllMaterials(completionBlock: @escaping (Result<[Material], Error>) -> Void){
        db.collection("materials").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let materials = documents.map { try? $0.data(as: Material.self)}.compactMap{ $0 }
            completionBlock(.success(materials))
        }
    }
    
    func deleteMaterial(idMaterial:String){
        db.collection("materials").document(idMaterial).delete()
    }
    
    func activateMaterial(idMaterial:String){
      db.collection("materials").document(idMaterial).setData(["active":true], merge:true)
    }
    
    func desactivateMaterial(idMaterial:String){
        db.collection("materials").document(idMaterial).setData(["active":false], merge:true)
    }
    
    func updateMaterial(idMaterial:String, name:String){
        db.collection("materials").document(idMaterial).setData(["name":name], merge:true)
    }
    
    func createMaterial(name:String, active:Bool){
        db.collection("materials").document().setData(["name":name, "active": active]) { err in
            if let err = err {
                print("Error creando la sala \(err)")
            }else{
                print("La sala ha sido creada con éxito")
            }
        }
    }
}
