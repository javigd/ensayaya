import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Voucher: Decodable, Identifiable{
    @DocumentID var id:String?
    let name:String
    let price:Double
    let usages:Int
}

struct Uvoucher: Decodable, Identifiable{
    @DocumentID var id:String?
    let name:String
    let usages:Int
    let numUsages:Int
}

final class VoucherDataSource{
    
    var db = Firestore.firestore()
    
    func getAllVouchers(completionBlock: @escaping (Result<[Voucher], Error>) -> Void){
        db.collection("vouchers").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let vouchers = documents.map { try? $0.data(as: Voucher.self)}.compactMap{ $0 }
            completionBlock(.success(vouchers))
        }
    }
    
    func updateVoucher(idVoucher:String, name:String, usages:Int, price:Double){
        db.collection("vouchers").document(idVoucher).setData(["name": name, "usages":usages, "price":price])
    }
    
    func deleteVoucher(idVoucher:String){
        db.collection("vouchers").document(idVoucher).delete()
    }
    
    func createVoucher(usages:Int, name:String, price:Double){
        db.collection("vouchers").document().setData(["usages":usages, "name":name, "price":price])
    }
    
    func buyVoucher(idUser:String, usages:Int, name:String){
        db.collection("users").document(idUser).collection("uVoucher").document().setData(["numUsages":usages, "usages": usages, "name":name])
    }
    
    func voucherPerId(idUser:String, completionBlock: @escaping (Result<[Uvoucher], Error>) -> Void){
        db.collection("users").document(idUser).collection("uVoucher").whereField("numUsages", isGreaterThan: 0).addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let uVouchers = documents.map { try? $0.data(as: Uvoucher.self)}.compactMap{ $0 }
            completionBlock(.success(uVouchers))
        }
    }
}
