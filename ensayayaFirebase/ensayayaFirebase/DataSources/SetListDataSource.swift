import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


struct SetList: Decodable, Identifiable, Hashable{
    
    @DocumentID var id:String?
    let idUser:String
    let canciones:[Cancion]?
    var duracionTotal:String{
        get{
            var acumuladorMin:Int = 0
            var acumuladorSec:Int = 0
            guard let a = canciones else {return "2:40"}
            for cancion in a {
                if cancion.duracion == ""{
                    return "2:40"
                }
                let minSec = cancion.duracion.split(separator: ":")
                let min = Int(minSec[0])
                let sec = Int(minSec[1])
                acumuladorMin = acumuladorMin + min!
                acumuladorSec = acumuladorSec + sec!
            }
            return String(format:"%d:%d", acumuladorMin + Int(acumuladorSec/60), acumuladorSec%60)
        }
    }
 }

struct Cancion: Decodable, Hashable{
    
    let titulo:String
    let duracion:String
}

final class SetListDataSource{
    
    var db = Firestore.firestore()
    
    func getSetList(idUser:String, completionBlock: @escaping (Result<[SetList], Error>) -> Void){
        
        db.collection("setLists").whereField("idUser", isEqualTo: UserDefaults.standard.string(forKey: "id") ?? "").addSnapshotListener {query, error in
            if let error = error{
                print("Error obteniendo los datos \(error.localizedDescription)")
                completionBlock(.failure(error))
                return
            }
            guard let documents = query?.documents.compactMap({ $0 }) else{
                completionBlock(.success([]))
                return
            }
            let setList = documents.map { try? $0.data(as: SetList.self)}.compactMap{ $0 }
            completionBlock(.success(setList))
        }
    }
    
    func deleteSetList(idSetList:String){
        db.collection("setLists").document(idSetList).setData(["idUser":UserDefaults.standard.string(forKey: "id") ?? ""])
    }
    
    func addSong(title:String, minutes:String, seconds:String, idSetList:String){
        let duration = minutes + ":" + seconds
        let cancion = Cancion(titulo: title, duracion: duration)

        db.collection("setLists").document(idSetList).setData(["idUser": UserDefaults.standard.string(forKey: "id") ?? "", "canciones": FieldValue.arrayUnion([[
            "titulo":title,
            "duracion":duration
        ]])], merge:true)
    }
    
    func removeSongs(songs:Set<String>, idSetList:String){
        let songsArray = Array(songs)
        for song in songsArray{
           let tituloDuracion = song.split(separator:"-")
            let titulo = tituloDuracion[0]
            let duracion = tituloDuracion[1]
            db.collection("setLists").document(idSetList).updateData(["canciones":FieldValue.arrayRemove([[
                "titulo":titulo,
                "duracion":duracion
            ]])])
        }
    }
}
