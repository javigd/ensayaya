import SwiftUI
import Firebase

class AppDelegate: NSObject, UIApplicationDelegate{
    func application(_ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions:
        [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        return true
      }
}

@main
struct ensayayaFirebaseApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate 
    @StateObject var authenticationViewModel = AuthenticationViewModel()
    
    //authenticationViewModel.user != nil --> Hay un usuario guardado en sesión
    //authenticationViewModel.user?.verified == true --> El usuario ha validado su correo electronico
    //UserDefaults.standard.bool(forKey:"active") == true --> La cuenta de usuario está deshabilitada
    
    var body: some Scene {
        WindowGroup {
            if (authenticationViewModel.user != nil && authenticationViewModel.user?.verified == true && UserDefaults.standard.bool(forKey:"active") == true){
                  HomeView().environmentObject(authenticationViewModel)
              }
            else if(authenticationViewModel.user != nil && authenticationViewModel.user?.verified == false){
                VerificateEmailView().environmentObject(authenticationViewModel)
            }
            else if(authenticationViewModel.user != nil && UserDefaults.standard.bool(forKey:"active") == false){
                DisabledAccountView().environmentObject(authenticationViewModel)
            }
            else if(authenticationViewModel.user == nil && authenticationViewModel.user?.verified == false){
                VerificateEmailView().environmentObject(authenticationViewModel)
            }
            else{
                SignInView().environmentObject(authenticationViewModel)
            }
        }
    }
}
